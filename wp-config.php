<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'intranet-wp-api' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'Generaltech' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#w8(K{iJW[?#)e6Ht`jqXF%%w[VR20]Q<]7EmxLQd1cU&k=2B7{5g@39^3Aw+bo/' );
define( 'SECURE_AUTH_KEY',  '^W/7hlg/`@vpduVgS?$a_2{F9]Ki7O[m8LO8j]W=YS79Vpnm/{:7-|Z VLG`,kt^' );
define( 'LOGGED_IN_KEY',    '[]r.W&#KSi]SvWKyWmu>XG/4izO%&;bZdr>5/eE:^hw6Y|y,A #yJ2~7##~b ki#' );
define( 'NONCE_KEY',        '134odnPGZas`cnD(J$F{v#3/ VU}}uiE#idedH]q5[}sJt+ 1YRa6-rZ;O~cx`2b' );
define( 'AUTH_SALT',        'tWYVc2~d1MW*idjlfka]U@2{E5_is*o[qXid[[!|Q-+H_rf!{ni|I3e-r97v(cX*' );
define( 'SECURE_AUTH_SALT', 'F|/`Fulz?aTbx=OneA4 ?5)0-(]@cCeCc2UL!&u%QV_3!->y_VuSG)]C2@Kxd+Y4' );
define( 'LOGGED_IN_SALT',   'zadg{kPVTZIX>[3;7/Hxl;_T~%6C(gQmoXBeaUD@jH[3>ZoK5:}M<d_{}?N!wq3B' );
define( 'NONCE_SALT',       '0%G<Oh]X+|flOCP8xf>#7(E#?8Tb`[t o9(V[qlfgq(UJO bEeL,[p4H[Xmb.j Z' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');