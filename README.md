# Instalação local do WordPress 5.1.1

## Dados de cadastro

### Banco de dados

**host** localhost

**usuario** root

**senha** (senha local)

**database** intranet-wp-api

**prefixo de tabela** _wp

### Adinistrativo

**título** API Intranet

**usuario** root

**senha** iLL?ob3YHCHX

**email** inema@filipelopes.me

### Editor de teste

**usuário** editor_teste  
**senha** \*%6sPnuOCf\^7xW&t#musYRSu  
**email** filiperocklopes@gmail.com

### Publicação de teste

**usuário** publicacao_teste  
**senha** 3t7ZO$mkmaKX5uDAA7dC6P(p  
**email** filiperolopes@gmail.com

# Modo de utilização em desenvolvimento

Rodar servidor embutido php:
```bash
php -S localhost:9000
```

Observação de configuração do servidor: **A nova versão do WORDPRESS roda com a configuração abaixo para versão de php mais antiga**

```ini
; php.ini
always_populate_raw_post_data = -1
```

# Servidor Debian 9 (VPS)

## Configurações do VPS

Porta de escuta: 172.16.0.88:80  
Usuario: root  
Senha: ***@2019  

## Pacotes instalados:

* Apache2
* default-mysql-server
* php 7
* curl

### Códigos utilizados
```bash
# https://www.cyberciti.biz/faq/how-to-install-linux-apache-mysql-php-lamp-stack-on-debian-9-stretch/
apt install apache2
apt install default-mysql-server
mysql_secure_installation
# Set root password? [Y/n] Y
# New password: mari@db_pass
# Remove anonymous users? [Y/n] Y
# Disallow root login remotely? [Y/n] n
# Remove test database and access to it? [Y/n] Y
# Reload privilege tables now? [Y/n] Y
apt install php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-gd php7.0-opcache
# Installs:
# 1. PHP version 7 for Apache 2
# 2. PHP 7 mysql connectivity module
# 3. PHP 7 OpCache module to speed up scripts
# 4. PHP 7 GD module for graphics
apt-get install phpmyadmin
# Web server to reconfigure automatically: 'apache2'
# Configure database for phpmyadmin with dbconfig-common? <Yes>
# MySQL application password for phpmyadmin: Pm@_pass
# > http://172.16.0.88/phpmyadmin > NOT FOUND
vi /etc/apache2/apache2.conf
# Adicionar linhas seguintes:
# > # Include phpMyAdmin configuration
# > Include /etc/phpmyadmin/apache.conf
mysql -u root -p
```

```sql
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

### Criação de novo Virtual Host

Criação de Virtual Host na porta 8081 para API do Painel Ambiental

```conf
# /etc/apache2/ports.conf
Listen 8081
# /etc/apache2/sites-enabled/api-painel-ambiental.conf
<VirtualHost *:8081>

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/api-painel-ambiental

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
# /etc/apache2/apache2.conf
<Directory /var/www/>
	Options Indexes FollowSymLinks
	AllowOverride All
	Require all granted
</Directory>
```

### CONfigurações de Banco para API Painel

**Host** 172.16.0.41  
**Host** brumado.inema.intranet  
**db** meioambiente  
**user** postgres  
**password** 123456

## Nginx

```bash
apt install ufw
ufw allow OpenSSH
ufw enable
apt-get update
systemctl stop apache2
apt-get install nginx
sudo ufw allow 'Nginx HTTP'
```

**Atenção**  
O ufw aparentemente não está permitindo o nginx, então deixei desabilitado.

Configurações de porta modificadas para 8080 em /etc/nginx/sites-available/default

## Configurações de instalação

### MariaDB e PHPmyAdmin  

MariaDB user: root  
```bash
mysql -u root -p
```  
MariaDB password:  mari@db_pass  
PHPmyAdmin (phpmyadmin) password: Pm@_pass

### WORDPRESS Duplicator config

Action: **Create New Database**  
Host: **localhost**  
Database: **intranet-wp-api**  
User: **phpmyadmin**  
Password: **Pm@_pass**  

# Instalações posteriores necessárias

É necessário instalar o ImageMagick para rodar a função de capturar cor dominante das imagens
```bash
apt-get install php-imagick
# php -m | grep imagick
service apache2 restart
```

# 5 - Manual de Implantação

## 5.1 - Bibliotecas comuns

Não há pacotes comuns há serem instalados

## 5.2 - Instalar e configurar servidor NGINX

```bash
# ufw - firewall do nginx
apt install ufw
ufw allow OpenSSH
ufw enable
apt-get update
systemctl stop apache2
apt-get install nginx
sudo ufw allow 'Nginx HTTP'
```

O Firewall estava impedindo as multiplas portas e precisava ser reconfigurado, por questão de tempo preferi desabilitar
```bash
ufw disable
```

### Arquivos de configuração

/etc/nginx/sites-enabled/api-painel-ambiental
```conf
# Configuração do backend em WORDPRESS
server {
	listen 8081;
	listen [::]:8081;

	server_name api_painel_ambiental;

	root /var/www/api-painel-ambiental;
	index index.php;

    error_log /var/log/nginx/api-painel-ambiental.error.log;
    access_log /var/log/nginx/api-painel-ambiental.access.log;

    location ~ /\.ht {
      deny all;
    }

	location / {
        try_files $uri /src/public/index.php$is_args$args;
    }    

    location ~ \.php$ {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```  
  
/etc/nginx/sites-enabled/wordpress
```conf
# Configuração do backend em WORDPRESS
server {
	listen 8080;
	listen [::]:8080;

	server_name wordpress_intranet_meioambiente;

	root /var/www/wordpress;
	index index.php;

    access_log /var/log/nginx/intranet-wordpress.access.log;
  	error_log /var/log/nginx/intranet-wordpress.error.log;

	location / {
		# try_files $uri $uri/ =404;
		# http://v2.wp-api.org/guide/problems/#query-parameters-are-ignored
    	try_files $uri $uri/ /index.php$is_args$args;
		add_header Access-Control-Allow-Origin *;
		autoindex on;
	}

	location ~ ^/wp-json/ {
		# if permalinks not enabled
		rewrite ^/wp-json/(.*?)$ /?rest_route=/$1 last;
	}

	location ~ \.php$ {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```  
  
/etc/nginx/sites-enabled/frontend
```conf
# Configurações do frontend react 
server {
	listen 80;
	listen [::]:80;

	root /var/www/frontend;

	# Add index.php to the list if you are using PHP
	index index.html index.htm index.nginx-debian.html;

	server_name intranet_meioambiente;

	access_log /var/log/nginx/intranet-frontend.access.log;
  	error_log /var/log/nginx/intranet-frontend.error.log;

	location / {
        try_files $uri /index.html;
    }
}
```

## 5.3 - Instalar e configurar PHP7 e pacotes associados

Pacotes gerais gerais
```bash
apt-get install php7.0-fpm
```

Pacote de compilação para NGINX
```bash
apt-get install php7.0-fpm
```

Arquivo: /etc/php/7.0/fpm/php.ini
```ini
; cgi.fix_pathinfo=1
cgi.fix_pathinfo=0
```

Arquivo: /etc/php/7.0/fpm/pool.d/www.conf
```conf
; listen = /run/php/php7.0-fpm.sock
listen = 127.0.0.1:9000
```

## 5.4 - Instruções adicionais

Os arquivos de frontend serão gerados através do `yarn build` com os arquivos clonados do repositório de referência após o devido `yarn install` para instalação de dependências.  
  
Os arquivos de gerenciados wordpress serão cedidos no formato zip juntamento com um instalador `installer.php`, os dois devem ser colocados na mesma pasta e o `installer.php` deve ser executado por um navegador e fornecidos os dados solicitados para dump do banco de dados. O "executável" desempacotará o arquivo zip no mesmo diretótio onde foi colado e instalará o banco de dados no host fornecido na instalação pelo navegador.




