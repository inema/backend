<?php

/* ----------
  Captura os anos disponíveis em documentos dependendo do ano de referência
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/anos/(?P<aba>[\w]+)', array(
    'methods' => 'GET',
    'callback' => function($data){
      global $wpdb;

      $aba = $data["aba"];

      switch ($aba) {
        case "calendario":
          $anos = $wpdb->get_results("SELECT DISTINCT EXTRACT(YEAR FROM `datetime`) FROM {$wpdb->prefix}adf_agenda ORDER BY `datetime` DESC ");
          break;
        case "docs_especificos":
          $anos = $wpdb->get_results("SELECT DISTINCT EXTRACT(YEAR FROM `data`) FROM {$wpdb->prefix}adf_docs_especificos ORDER BY `data` DESC ");
          break;
        case "relatorios_anuais":
          $anos = $wpdb->get_results("SELECT DISTINCT `ano` FROM {$wpdb->prefix}adf_outros_docs WHERE tipo = 'relatorios' ORDER BY `ano` DESC ");
          break;
        default:
          $anos = $wpdb->get_results("SELECT DISTINCT `ano` FROM {$wpdb->prefix}adf_outros_docs WHERE tipo = '{$aba}' ORDER BY `ano` DESC ");
          break;
      }      

      $retorno = array();

      foreach($anos as $obj_ano){
        foreach($obj_ano as $ano){
          array_push($retorno, $ano);
        }
      }

      return $retorno;
    },
  ));
});

/* ----------
  Captura os dados da agenda referente ao ano passado nos parâmetros
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/agenda/(?P<ano>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_agenda',
  ));
});

function get_agenda($data)
{
  global $wpdb;

  $agenda_ano = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_agenda WHERE EXTRACT(YEAR FROM `datetime`) = '{$data["ano"]}' AND b_del = 0 ORDER BY `datetime` DESC");

  return $agenda_ano;
}

/* ----------
  Captura os dados de CONVOCATORIAS referente ao ano passado nos parâmetros
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/convocatorias/(?P<ano>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_convocatorias',
  ));
});

function get_convocatorias($data)
{
  global $wpdb;

  $resultado = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE ano = {$data["ano"]} AND tipo = 'convocatorias' AND b_del = 0 ORDER BY `ano` DESC");

  return $resultado;
}

/* ----------
  Captura os dados de ATAS referente ao ano passado nos parâmetros
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/atas/(?P<ano>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_atas',
  ));
});

function get_atas($data)
{
  global $wpdb;

  $atas = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE EXTRACT(YEAR FROM `data`) = {$data["ano"]} AND tipo = 'atas' AND b_del = 0 ORDER BY `data` DESC");

  $resultado = array();

  foreach( $atas as $ata ){

    $imagens = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE tipo = 'registros_fotograficos' AND ata_ref = '{$ata->titulo}' AND b_del = 0 ORDER BY `data` DESC");

    $ata->imagens = $imagens;
    foreach( $ata->imagens as $img ){
      // Para ser utilizado no componente de galeria
      $img->original = $img->src;
      $img->thumbnail = $img->src;
    }
    
    array_push($resultado, $ata);

  }

  return $resultado;
}

/* ----------
  Captura os dados de MANUAIS referente ao ano passado nos parâmetros
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/manuais/(?P<ano>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_manuais',
  ));
});

function get_manuais($data)
{
  global $wpdb;

  $resultado = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE ano = {$data["ano"]} AND tipo = 'manuais' AND b_del = 0 ORDER BY `ano` DESC");

  return $resultado;
}

/* ----------
  Captura os dados de MANUAIS referente ao ano passado nos parâmetros
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/relatorios/(?P<ano>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_relatorios',
  ));
});

function get_relatorios($data)
{
  global $wpdb;

  $resultado = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE ano = {$data["ano"]} AND tipo = 'relatorios' AND b_del = 0 ORDER BY `ano` DESC");

  return $resultado;
}

/* ----------
  Captura os dados de COMUNICADOS referente ao ano passado nos parâmetros organizando por data
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/comunicados/(?P<ano>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_comunicados',
  ));
});

function get_comunicados($data)
{
  global $wpdb;

  $datas_comunicados = $wpdb->get_results("SELECT DISTINCT data FROM {$wpdb->prefix}adf_outros_docs WHERE EXTRACT(YEAR FROM `data`) = {$data["ano"]} AND tipo = 'comunicados' AND b_del = 0 ORDER BY `data` DESC");

  $resultado = array();
  $data_pesquisada = '';
  foreach( $datas_comunicados as $data ){
    // array_push($resultado, $data->data);
    $comunicados = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE data = '$data->data' AND tipo = 'comunicados' AND b_del = 0 ORDER BY `data` DESC");
    $resultado[$data->data] = array();
    foreach( $comunicados as $comunicado){
      array_push( $resultado[$data->data], $comunicado );
    }
  }

  return $resultado;
}

/* ----------
  Captura os dados de documentos específicos referente às informações passadas nos parâmetros
---------- */

add_action('rest_api_init', function () {
  register_rest_route('adf/v1', '/docs_especificos', array(
    'methods' => 'GET',
    'callback' => 'get_docs_especificos',
  ));
});

function get_docs_especificos($request)
{
  global $wpdb;
  $ano = $request["ano"];
  $grupo = $request["grupo"];
  $carreira = $request["carreira"];
  $tipo = $request["tipo"];
  $wp_uploads_url = wp_get_upload_dir()["baseurl"];

  $retorno = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_docs_especificos WHERE EXTRACT(YEAR FROM `data`) LIKE '%$ano%' AND grupo LIKE '%$grupo%' AND carreira LIKE '%$carreira%' AND tipo LIKE '%$tipo%' AND b_del = 0 ORDER BY `data` DESC");

  foreach( $retorno as $arquivo ){
    $arquivo->base_url = $wp_uploads_url;
  }

  return $retorno;
}

