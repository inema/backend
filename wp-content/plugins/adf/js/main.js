jQuery(document).ready(function ($) {

  $("#table_calendario").on("click", ".del_compromisso", function (e) {
    e.preventDefault();
    if(confirm("Tem certeza que deseja excluir esse compromisso?")){
      window.location.href = this.href;
    }
  })

  function closeModals() {
    $(".backdrop").hide();
    $(".modal").hide();
    $(".wrap").removeClass("blured");
  }

  closeModals();

  function openModalAddCompromisso() {
    $("#backdrop").show();
    $("#modal_add_compromisso").show();
    $(".wrap").addClass("blured")
  }

  $(".backdrop").click( function() {
    closeModals();
  })

  $("#adicionar_compromisso").click( function() {
    console.log("oi")
    openModalAddCompromisso();
  })

  $("#table_docs_wrapper").on("click", ".del_item", function (e) {
    e.preventDefault();
    if(confirm("Tem certeza que deseja excluir esse item?")){
      window.location.href = this.href;
    }
  })

  /* ---------- DOCUMENTOS ESPECÍFICOS ---------- */
  
  $("#grupo_ocupacional").change( function(){
    grupos.forEach( (e, i) => {
      if($("#grupo_ocupacional").val() == e ){
        content = `<option value="" selected disabled>Selecione uma carreira</option>`;
        carreira_por_grupo[i].forEach( el => {
          content += `<option value="${el}">${el}</option>`;
        })
        $("#carreira").html(content)
      }
    })
  })

  grupos = [
    "Comunicação Social",
    "Fiscalização e Regulação",
    "Gestão Pública",
    "Técnico Específico",
    "Técnico Administrativo"
  ]

  carreira_por_grupo = [
    [ "Jornalista" ],
    [ "Especialista em Meio Ambiente e Recursos Hídricos", "Técnico em Meio Ambiente e Recursos Hídricos" ],
    [ "Especialista em Políticas Públicas e Gestão Governamental" ],
    [ "Médico Veterinário" ],
    [ "Analista Técnico" ]
  ]

})