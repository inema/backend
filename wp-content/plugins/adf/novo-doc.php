<?php

/**
 * DOCUMENTOS ESPECÍFICOS
 */

if(isset($_POST["docs_especificos"])):
global $wpdb;

$data = $_POST["data"];
$grupo = $_POST["grupo_ocupacional"];
$carreira = $_POST["carreira"];
$tipo = $_POST["tipo"];

if($_FILES["arquivo"]['size']){
    $errors= array();
    $file_name = $_FILES['arquivo']['name'];
    $file_size =$_FILES['arquivo']['size'];
    $file_tmp =$_FILES['arquivo']['tmp_name'];
    $file_type=$_FILES['arquivo']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['arquivo']['name'])));

    $extensions= array("pdf");

    // echo "FILE EXT '{$file_ext}'";

    if(in_array($file_ext,$extensions)=== false){
        $errors[]="extension not allowed, please choose a PDF file.";
    }

    if($file_size > (2097152/2)*10){
        $errors[]='File size must be excately 10 MB';
    }
    echo "UPLOADS_DIR  '{$wp_uploads_dir}'";
    if(empty($errors)==true){
        move_uploaded_file($file_tmp, $wp_uploads_dir."/adf/doc/especificos/".$file_name);
        // echo "Success";
    }else{
        echo "Errors:";
        print_r($errors);
        // echo "\n";
    }

    $src = "adf/doc/especificos/".$file_name;
}

$wpdb->query(" 
    INSERT INTO `{$wpdb->prefix}adf_docs_especificos` (
    `id` ,
    `data` ,
    `grupo` ,
    `carreira`,
    `tipo`,
    `src`
    )
    VALUES (
    NULL ,  '$data',  '$grupo', '$carreira', '$tipo', '$src'
    );")or die($wpdb->error);

echo "<span class='success'>Documento adicionado com sucesso</span>";

endif;

/**
 * OUTROS DOCUMENTOS
 */

if( isset($_POST["convocatorias"]) || isset($_POST["atas"]) || isset($_POST["manuais"]) || isset($_POST["relatorios"]) || isset($_POST["registros_fotograficos"]) || isset($_POST["comunicados"])):
global $wpdb;

$ano = isset($_POST["ano"]) ? $_POST["ano"] : NULL;

$tipo = "";

if(isset($_POST["convocatorias"])){
    $tipo = "convocatorias";
}

if(isset($_POST["atas"])){
    $tipo = "atas";
}

if(isset($_POST["manuais"])){
    $tipo = "manuais";
}

if(isset($_POST["relatorios"])){
    $tipo = "relatorios";
}

if(isset($_POST["registros_fotograficos"])){
    $tipo = "registros_fotograficos";
}

if(isset($_POST["comunicados"])){
    $tipo = "comunicados";
}

$data = isset($_POST["data"]) ? $_POST["data"] : NULL;
$titulo = isset($_POST["titulo"]) ? $_POST["titulo"] : NULL;
$assunto = isset($_POST["assunto"]) ? $_POST["assunto"] : NULL;
$descricao = isset($_POST["descricao"]) ? $_POST["descricao"] : NULL;
$ata_ref = isset($_POST["ata"]) ? $_POST["ata"] : NULL;

if(is_array($titulo)){
    /**
     * FOI PEDIDO DE ÚLTIMA HORA PARA ADICIONAR AO BACKLOG COM MUITA DEMANDA PRÉVIA. Se tem vários títulos é porque eu devo dar um loop e fazer várias inserções para cada uma
     * EU SEI, muito código repetido.
     */

    for ($i=0; $i < count($titulo); $i++) {
        //  echo $titulo[$i];
        //  print_r($_FILES["arquivo"]);

        if($_FILES["arquivo"]['size'][$i]){
            $errors= array();
            $file_name = $_FILES['arquivo']['name'][$i];
            $file_size =$_FILES['arquivo']['size'][$i];
            $file_tmp =$_FILES['arquivo']['tmp_name'][$i];
            $file_type=$_FILES['arquivo']['type'][$i];
            $file_ext=strtolower(end(explode('.',$_FILES['arquivo']['name'][$i])));

            $extensions= array("jpeg", "jpg", "png", "pdf");
        
            // echo "FILE EXT '{$file_ext}'";
        
            if(in_array($file_ext,$extensions)=== false){
                $errors[]="Extensão inválida. Favor selecionar um tipo de arquivo permitido";
            }
        
            if($file_size > (2097152/2)*10){
                $errors[]='File size must be excately 10 MB';
            }
            // echo "UPLOADS_DIR  '{$wp_uploads_dir}'";
            if(empty($errors)==true){
                move_uploaded_file($file_tmp, $wp_uploads_dir."/adf/doc/{$tipo}/".$file_name);
                echo "<span class='success'>Documento adicionado com sucesso</span>";
                // echo "Success";
            }else{
                echo "Errors:";
                print_r($errors);
                // echo "\n";
            }

            $src = $wp_uploads_url."/adf/doc/{$tipo}/".$file_name;

            $values = $ano == NULL ? "VALUES (
                NULL ,  NULL, '$data', '$tipo', '$ata_ref', '$src', '{$titulo[$i]}', '$assunto', '$descricao'
                )" : "VALUES (
                NULL ,  '$ano', '$data', '$tipo', '$ata_ref', '$src', '{$titulo[$i]}', '$assunto', '$descricao'
                )";
        
            $wpdb->query(" 
                INSERT INTO `{$wpdb->prefix}adf_outros_docs` (
                `id` ,
                `ano` ,
                `data` ,
                `tipo` ,
                `ata_ref` ,
                `src` ,
                `titulo` ,
                `assunto` ,
                `descricao`
                )
                ".$values.";")or die($wpdb->error);
        }
    }

}else{

// print_r($_FILES["arquivo"]);

if($_FILES["arquivo"]['size']){
    $errors= array();
    $file_name = $_FILES['arquivo']['name'];
    $file_size =$_FILES['arquivo']['size'];
    $file_tmp =$_FILES['arquivo']['tmp_name'];
    $file_type=$_FILES['arquivo']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['arquivo']['name'])));

    if($tipo == "registros_fotograficos"){
        $extensions= array("jpeg", "jpg", "png");
    }else{
        $extensions= array("pdf");
    }

    // echo "FILE EXT '{$file_ext}'";

    if(in_array($file_ext,$extensions)=== false){
        $errors[]="Extensão inválida. Favor selecionar um tipo de arquivo permitido";
    }

    if($file_size > (2097152/2)*10){
        $errors[]='File size must be excately 10 MB';
    }
    // echo "UPLOADS_DIR  '{$wp_uploads_dir}'";
    if(empty($errors)==true){
        move_uploaded_file($file_tmp, $wp_uploads_dir."/adf/doc/{$tipo}/".$file_name);
        echo "<span class='success'>Documento adicionado com sucesso</span>";
        // echo "Success";
    }else{
        echo "Errors:";
        print_r($errors);
        // echo "\n";
    }
}

$src = $wp_uploads_url."/adf/doc/{$tipo}/".$file_name;

$values = $ano == NULL ? "VALUES (
    NULL ,  NULL, '$data', '$tipo', '$ata_ref', '$src', '$titulo', '$descricao'
    )" : "VALUES (
    NULL ,  '$ano', '$data', '$tipo', '$ata_ref', '$src', '$titulo', '$descricao'
    )";

$wpdb->query(" 
    INSERT INTO `{$wpdb->prefix}adf_outros_docs` (
    `id` ,
    `ano` ,
    `data` ,
    `tipo` ,
    `ata_ref` ,
    `src` ,
    `titulo` ,
    `descricao`
    )
    ".$values.";")or die($wpdb->error);
}

endif;



?>