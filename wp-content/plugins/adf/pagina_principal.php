<?php

$current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$main_page_url = get_admin_url(null, '', 'admin') . "admin.php?page=adf";
global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'excluir-processar.php');

$wp_uploads_url = wp_get_upload_dir()["baseurl"];
$wp_uploads_dir = wp_get_upload_dir()["basedir"];

$userID = get_current_user_id();

include_once(plugin_dir_path(__FILE__) . 'novo-doc.php');

$subpage = "convocatorias";

function pageClass($aba){
    if($aba == $_GET["subpage"]){
        echo "active";
    }
}

if(isset($_GET["subpage"])){
    $subpage = $_GET["subpage"];

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>

    <section>
        <ul id="abas">
            <a href="<?=$main_page_url."&subpage=convocatorias"?>"><li class="<?=pageClass('convocatorias')?>">Convocatórias</li></a>
            <a href="<?=$main_page_url."&subpage=atas"?>"><li class="<?=pageClass('atas')?>">Atas</li></a>
            <a href="<?=$main_page_url."&subpage=manuais"?>"><li class="<?=pageClass('manuais')?>">Manuais</li></a>
            <a href="<?=$main_page_url."&subpage=relatorios"?>"><li class="<?=pageClass('relatorios')?>">Relatórios</li></a>
            <a href="<?=$main_page_url."&subpage=registros_fotograficos"?>"><li class="<?=pageClass('registros_fotograficos')?>">Registros fotográficos</li></a>
            <a href="<?=$main_page_url."&subpage=comunicados"?>"><li class="<?=pageClass('comunicados')?>">Comunicados</li></a>
            <a href="<?=$main_page_url."&subpage=documentos_especificos"?>"><li class="<?=pageClass('documentos_especificos')?>">Legislação específica</li></a>
        </ul>
        <div id="conteudo">
            <?php
            if($subpage == 'documentos_especificos'){
            ?>
                <form method="post" enctype="multipart/form-data">
                    <input type="hidden" name="docs_especificos"/>
                    <label for="grupo_ocupacional">Grupo Ocupacional</label>
                    <select name="grupo_ocupacional" id="grupo_ocupacional" class="mb">
                        <option value="" selected>Selecione um Grupo Ocupacional</option>
                        <option value="Comunicação Social">Comunicação Social</option>
                        <option value="Fiscalização e Regulação">Fiscalização e Regulação</option>
                        <option value="Gestão Pública">Gestão Pública</option>
                        <option value="Técnico Específico">Técnico Específico</option>
                        <option value="Técnico Administrativo">Técnico Administrativo</option>
                    </select>

                    <label for="carreira">Carreira</label>
                    <select name="carreira" id="carreira" class="mb">
                        <option>Selecione uma carreira</option>
                    </select>

                    <label for="tipo">Tipo de Documento</label>
                    <select name="tipo" id="tipo" class="mb">
                        <option>Selecione um tipo de documento</option>
                        <option value="Legislação">Leis</option>
                        <option value="Decretos">Decretos</option>
                        <option value="Instruções Normativas">Instruções Normativas</option>
                        <option value="Portarias">Portarias</option>
                    </select>

                    <label for="data">Data do documento</label>
                    <input type="date" name="data" id="data" class="mb" value="<?=date("Y-m-j")?>"/>

                    <label for="arquivo">Documento em PDF</label>
                    <input type="file" name="arquivo" id="arquivo" required/>
                    <p>Serão aceitos apenas arquivos *.pdf</p>

                    <button type="submit" class="button button-primary">Adicionar</button>
                </form><br/><br/>
                <table id="table_docs_wrapper" class="display">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Grupo Ocupacional</th>
                            <th>Carreira</th>
                            <th>Tipo de Doc.</th>
                            <th>Abrir doc.</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $docs_especificos = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_docs_especificos WHERE `b_del` = 0 ORDER BY `data` ASC");

                        foreach ($docs_especificos as $doc) {
                            $date_obj = new DateTime($doc->data);
                            $doc_data = $date_obj->format('d/m/Y');
                            echo "<tr>";
                            echo "<td>{$doc_data}</td>";
                            echo "<td>{$doc->grupo}</td>";
                            echo "<td>{$doc->carreira}</td>";
                            echo "<td>{$doc->tipo}</td>";
                            echo "<td><a href='{$wp_uploads_url}/{$doc->src}' target='_blank' >Arquivo</a></td>";
                            echo "<td><a class=\"del_item\" href=\"{$current_url}&id={$doc->id}\">Excluir</a></td>";
                        }
                        ?>
                    </tbody>
                </table>
                <script>
                    jQuery(document).ready(function($) {
                        $('#table_docs_wrapper').DataTable();
                    });
                </script>
            <?php
            }else{
                ?>
                <form method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?=$subpage?>"/>

                    <?php
                    if( $subpage != "registros_fotograficos" && $subpage != "atas" && $subpage != "comunicados" ){
                        /**
                         *  documentos com apenas ano, sem data específica 
                         */
                    ?>
                    <label for="ano">Ano</label>
                    <select name="ano" id="no" class="mb" required>
                        <option value="" selected>Selecione um ano</option>
                        <?php
                            $intervalo_anos = 50;
                            $ano_inicio = date("Y") - $intervalo_anos;
                            $ano_fim = date("Y") + $intervalo_anos;
                            echo $ano_fim;
                            for ($i=$ano_inicio; $i < $ano_fim; $i++) { 
                                echo "<option value='{$i}'>{$i}</div>";
                            }
                        ?>                        
                    </select>                    

                    <label for="titulo">Título</label>
                    <input type="text" name="titulo" id="titulo" required/>

                    <label for="arquivo">Documento em PDF</label>
                    <input type="file" name="arquivo" id="arquivo" required/>
                    <p>Serão aceitos apenas arquivos *.pdf</p>

                    <?php
                    }

                    if( $subpage == "atas" || $subpage == "comunicados"){
                        /**
                         * Tipo de documento com data completa de diferencial e adicionado de última hora comunicados com possibilidade de 3 anexos
                         */
                    ?>
                        <label for="data">Data do documento</label>
                        <input type="date" name="data" id="data" class="mb" value="<?=date("Y-m-j")?>" required/>

                        
                        <?php
                        if($subpage == "atas"){
                        ?>
                            <label for="titulo">Título</label>
                            <input type="text" name="titulo" id="titulo" required/>

                            <label for="arquivo">Documento em PDF</label>
                            <input type="file" name="arquivo" id="arquivo" required/>
                            <p>Serão aceitos apenas arquivos *.pdf</p>
                        <?php
                        }else{
                        ?>
                        <label for="assunto">Assunto</label>
                        <input type="text" name="assunto" id="assunto" size="20" required/>

                        <fieldset>
                            <legend>Anexo 1</legend>
                            <label for="titulo_1">Título</label>
                            <input type="text" name="titulo[]" id="titulo_1" required/>

                            <label for="arquivo[]">Arquivo</label>
                            <input type="file" name="arquivo[]" id="arquivo_1" required/>
                        </fieldset>

                        <fieldset>
                            <legend>Anexo 2</legend>
                            <label for="titulo_2">Título</label>
                            <input type="text" name="titulo[]" id="titulo_2"/>

                            <label for="arquivo[]">Arquivo</label>
                            <input type="file" name="arquivo[]" id="arquivo_2"/>
                        </fieldset>

                        <fieldset>
                            <legend>Registro de envio</legend>
                            <p>Adicione aqui o <i>printscreen</i> do envio de email</p>
                            <label for="titulo_3">Título</label>
                            <input type="text" name="titulo[]" id="titulo_3" required/>

                            <label for="arquivo[]">Arquivo</label>
                            <input type="file" name="arquivo[]" id="arquivo_3" required/>
                        </fieldset>
                        <?php
                        }
                        ?>

                    <?php
                    }
                    ?>

                    <?php
                    if( $subpage == "registros_fotograficos" ){

                        $titulo_atas = $wpdb->get_col("SELECT titulo FROM {$wpdb->prefix}adf_outros_docs WHERE tipo = 'atas' AND titulo != 'NULL' AND `b_del` = 0 ORDER BY `titulo` ASC");
                        $source_autocoplete_ata = json_encode($titulo_atas);
                    ?>
                    
                    <!-- Para imagens  -->

                    <label for="titulo">Título</label>
                    <input type="text" name="titulo" id="titulo"/>

                    <label for="data">Data do documento</label>
                    <input type="date" name="data" id="data" class="mb" value="<?=date("Y-m-j")?>"/>

                    <label for="ata">Ata</label>
                    <input type="text" name="ata" id="ata" class="mb" required/>

                    <script>
                        jQuery(document).ready(function ($) {
                            $("#ata").autocomplete({
                                source: <?=$source_autocoplete_ata?>
                            })
                        })
                    </script>

                    <label for="arquivo">Documento em PDF</label>
                    <input type="file" name="arquivo" id="arquivo" required/>
                    <p>Serão aceitos apenas arquivos *.jpg *.jpeg *.png</p>
                    
                    <?php wp_editor("", "descricao", array(
                        "media_buttons" => false,
                        "textarea_rows" => 5
                    )); ?><br/>

                    <?php
                    }
                    ?>

                    <button type="submit" class="button button-primary">Adicionar</button>
                </form>
                <br/><br/>
                <table id="table_docs" class="display">
                    <thead>
                        <tr>
                            <th>Ano</th>
                            <th>Data</th>
                            <th>Título</th>
                            <th>Descrição</th>
                            <th>Abrir doc.</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $docs_especificos = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_outros_docs WHERE `b_del` = 0 AND tipo = '$subpage' ORDER BY `data` ASC");

                        foreach ($docs_especificos as $doc) {

                            if($doc->data){
                                $date_obj = new DateTime($doc->data);
                                $doc_data = $date_obj->format('d/m/Y');
                            }else{
                                $doc_data = "";
                            }

                            $array_src = explode("/", $doc->src);

                            echo "<tr>";
                            echo "<td>{$doc->ano}</td>";
                            echo "<td>{$doc_data}</td>";
                            echo "<td>{$doc->titulo}</td>";
                            echo "<td>{$doc->descricao}</td>";
                            echo "<td><a href='{$doc->src}' target='_blank' >";
                            echo end($array_src);
                            echo "</a></td>";
                            echo "<td><a class=\"del_item\" href=\"{$current_url}&id={$doc->id}\">Excluir</a></td>";
                        }
                        ?>
                    </tbody>
                </table>
                <script>
                    jQuery(document).ready(function($) {
                        $('#table_docs').DataTable();
                    });
                </script>
                <?php
            }
            ?>
        </div>
    </section>
</div>

<?php
    }else{
?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>

    <section>
        <ul id="abas">
            <a href="<?=$main_page_url."&subpage=convocatorias"?>"><li class="<?=pageClass('convocatorias')?>">Convocatórias</li></a>
            <a href="<?=$main_page_url."&subpage=atas"?>"><li class="<?=pageClass('atas')?>">Atas</li></a>
            <a href="<?=$main_page_url."&subpage=manuais"?>"><li class="<?=pageClass('manuais')?>">Manuais</li></a>
            <a href="<?=$main_page_url."&subpage=relatorios"?>"><li class="<?=pageClass('relatorios')?>">Relatórios</li></a>
            <a href="<?=$main_page_url."&subpage=registros_fotograficos"?>"><li class="<?=pageClass('registros_fotograficos')?>">Registros fotográficos</li></a>
            <a href="<?=$main_page_url."&subpage=comunicados"?>"><li class="<?=pageClass('comunicados')?>">Comunicados</li></a>
            <a href="<?=$main_page_url."&subpage=documentos_especificos"?>"><li class="<?=pageClass('documentos_especificos')?>">Legislação específica</li></a>
        </ul>
        <div id="conteudo">
            <p>Selecione uma das abas acima para começar</p>
        </div>
    </section>
</div>

<?php
    }
?>