<?php

// Adicionar scripts para rodar no dashboard
$hook_list_adf = array(
    "toplevel_page_adf",
    "sistema-adf_page_agenda",
);

function plugin_adf_add_scripts_0125($hook) {
    // print_r($hook); // toplevel_page_adf
    global $hook_list_adf;
    if( !in_array($hook, $hook_list_adf) ) return;
    wp_enqueue_script('plugin_adf_datatables', plugins_url('/js/DataTables/datatables.js', __FILE__), array('jquery'), '1.0', true);
    wp_enqueue_script('plugin_adf_main_script', plugins_url('/js/main.js', __FILE__), array( 'jquery',
    'jquery-ui-autocomplete' ), '1.0', true);
}

add_action('admin_enqueue_scripts', 'plugin_adf_add_scripts_0125');

function plugin_adf_add_style_2354($hook) {
    global $hook_list_adf;
    if( !in_array($hook, $hook_list_adf) ) return;
    wp_enqueue_style('plugin_adf_datatables', plugins_url('/css/DataTables/datatables.css', __FILE__));
    wp_enqueue_style('plugin_adf_main_style', plugins_url('/css/main.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'plugin_adf_add_style_2354' );

?>