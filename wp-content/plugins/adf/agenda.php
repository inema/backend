<?php

$current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$main_page_url = get_admin_url(null, '', 'admin') . "admin.php?page=agenda";
global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'excluir-processar.php');
include_once(plugin_dir_path(__FILE__) . 'novo-compromisso.php');

$userID = get_current_user_id();

?>

<div id="backdrop" class="backdrop"></div>
<div id="modal_add_compromisso" class="modal">
    <h3>Adicionar Compromisso</h3>
    <form method="post">
    <label for="data">Data:</label>
    <input type="date" name="data" id="data" required/>
    <label for="hora">Hora:</label>
    <input type="time" name="hora" id="hora" language="pt-br" required/>
    <label for="local">Local:</label>
    <input type="text" name="local" id="local" required/>
    <?php wp_editor("", "descricao", array(
                        "media_buttons" => false
                    )); ?><br/>
    <button type="submit" class="button button-primary">Adicionar</button>
    </form>
</div>

<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1><a id="adicionar_compromisso" href="#" class="page-title-action">
        Adicionar compromisso</a><br />
    <p>Essas são os álbuns existentes, para editar ou excluir favor acessar na tabela a coluna de opções:</p>

    <form method="post" id="form_add_galerias" enctype="multipart/form-data">

        <table id="table_calendario" class="display">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Horário</th>
                    <th>Local</th>
                    <th>Descrição</th>
                    <th>Excluir</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $agenda = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}adf_agenda WHERE `b_del` = 0 ORDER BY `datetime` DESC");

                foreach ($agenda as $evento) {
                    $datetime_obj = new DateTime($evento->datetime);
                    $evento_data = $datetime_obj->format('Y-m-d');
                    $evento_hora = $datetime_obj->format('H:i');
                    echo "<tr>";
                    echo "<td>{$evento_data}</td>";
                    echo "<td>{$evento_hora}</td>";
                    echo "<td>{$evento->local}</td>";
                    echo "<td><span class=\"more\"> <a class=\"ver_mais\">Ver</a>
                    <div>
                        {$evento->descricao}
                    </div>
                    </span></td>";
                    echo "<td><a class=\"del_compromisso\" href=\"{$current_url}&id={$evento->id}$tipo=agenda\">Excluir</a></td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function($) {
                $('#table_calendario').DataTable({
                    "order" : [[0, "desc"]]
                });
            });
        </script>
    </form>
</div>