<?php

// Roles AND capabilities
function wporg_adf_role()
{
    add_role(
        'editor_adf',
        'Integrante ADF',
        [
            'read'         => true,
            'edit_posts'   => true,
            'upload_files' => true,
        ]
    );
}
add_action('init', 'wporg_adf_role', 10);

function wporg_adf_role_caps()
{
    // gets the simple_role role object
    $role = get_role('editor_adf');
 
    // add a new capability
    $role->add_cap('ascom_adf', true);
}
 
// add simple_role capabilities, priority must be after the initial role definition
add_action('init', 'wporg_adf_role_caps', 11);

?>