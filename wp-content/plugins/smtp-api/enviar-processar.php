<?php

if (isset($_GET["enviar"])) :

    global $wpdb;
    
    $host = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}smtp_api WHERE chave = 'host' ");
    $remetente_teste = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}smtp_api WHERE chave = 'remetente_teste' ");

    $wpdb->show_errors();

    require_once("phpmailer/class.phpmailer.php");

    $corpo_texto = "<p>" .
        "<b>E-mail de Teste:</b> <br />" .
        "<b>Mensagem: </b> Teste de email <br />" .
        "<b>Mensagem: </b> Enviada às: " . date("d-m-Y H:i:s") . " <br />" .
        "<p>";
    $mail = new phpmailer();
    $mail->IsSMTP();
    $mail->HeaderLine("Content-type", "text/html");
    $mail->HeaderLine("charset", "utf-8");
    $mail->CharSet = "utf-8";
    $mail->SetLanguage('br');
    $mail->From = "nao_responda@inema.ba.gov.br";
    $mail->FromName = "Email de Teste - INTRANET";
    $mail->Host = $host;
    $mail->AddAddress($remetente_teste, "Remetente de Teste");
    //$mail->AddCC('xxx@hotmail.com', 'Teste 02'); // Copia
    //$mail->AddBCC('yyy@gmail.com', 'Teste 03'); // Cópia Oculta
    $mail->WordWrap = 50;
    $mail->IsHTML(true);
    $mail->Subject = "Teste de envio de email - INTRANET";
    $mail->Body = $corpo_texto;
    $enviado = true;
    $enviado = $mail->Send();

    print_r($enviado);
    echo "<span class='success'>Email enviado com Sucesso!!!</span>";

endif;
