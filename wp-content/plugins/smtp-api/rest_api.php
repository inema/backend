<?php

// API WP
add_action('rest_api_init', function () {
  register_rest_route('smtp_api/v1', '/send', array(
    'methods' => 'POST',
    'callback' => 'send_email',
  ));
});

function send_email($data)
{
  global $wpdb;
  require_once("phpmailer/class.phpmailer.php");

  $host = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}smtp_api WHERE chave = 'host' ");
  $rmt = $wpdb->get_var("SELECT valor FROM {$wpdb->prefix}smtp_api WHERE chave = 'remetente' ");

  $motivo = $data["motivo"];
  $nome = $data["nome"];
  $email = $data["email"];
  $ramal = $data["ramal"];
  $mensagem = $data["mensagem"];

  $corpo_texto = "<p>" .
    "<b>Motivo:</b> {$motivo} <br/>" .
    "<b>E-mail:</b> {$email} <br/>" .
    "<b>Ramal:</b> {$ramal} <br/><br/>" .
    "<strong>Mensagem: </strong> {$mensagem} <br/><br/>" .
    "<b> {$nome} </b><br/>" .
    "<sub>Enviada às: " . date("d-m-Y H:i:s") . "</sub><br/>" .
    "<p>";

  $mail = new phpmailer();
  $mail->IsSMTP();
  $mail->HeaderLine("Content-type", "text/html");
  $mail->HeaderLine("charset", "utf-8");
  $mail->CharSet = "utf-8";
  $mail->SetLanguage('br');
  $mail->From = "nao_responda@inema.ba.gov.br";
  $mail->FromName = "Fale Conosco - Intranet";
  $mail->Host = $host;
  $mail->AddAddress($rmt, "Remetente");
  //$mail->AddCC('xxx@hotmail.com', 'Teste 02'); // Copia
  //$mail->AddBCC('yyy@gmail.com', 'Teste 03'); // Cópia Oculta
  $mail->WordWrap = 50;
  $mail->IsHTML(true);
  $mail->Subject = "[INTRANET] - Contato - {$motivo}";
  $mail->Body = $corpo_texto;
  $enviado = true;
  $enviado = $mail->Send();

  if($enviado){
    return array( 
      "status" => "Email enviado com sucesso"
    );
  }else{
    return array(
      "status" => "Falha no envio do email"
    );
  }
}
