<?php

// Roles AND capabilities
function wporg_banner_principal_role()
{
    add_role(
        'editor_banner_principal',
        'Editor do Banner Principal',
        [
            'read'         => true,
            'edit_posts'   => true,
            'upload_files' => true,
        ]
    );
}
add_action('init', 'wporg_banner_principal_role', 10);

function wporg_banner_principal_role_caps()
{
    // gets the simple_role role object
    $role = get_role('editor_banner_principal');
 
    // add a new capability
    $role->add_cap('ascom_banner_principal', true);
}
 
// add simple_role capabilities, priority must be after the initial role definition
add_action('init', 'wporg_banner_principal_role_caps', 11);

?>