<?php

$edit_url = get_admin_url(null, '', 'admin') . "admin.php?page=banner_principal-editar";

global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'editar-processar.php');

//  Caso NÃO tenha sido passado o id

if (!isset($_GET["id"])) {
    ?>
    <div class="wrap">
        <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
        <h2 class="title">Selecione uma das galerias abaixo que deseja editar</h2>
        <form method="post" id="form_add_galerias" enctype="multipart/form-data">

            <table id="table_imagens" class="display">
                <thead>
                    <tr>
                        <th>Banner</th>
                        <th>Link de redirecionamento</th>
                        <th>Carrousel</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $banners = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}banner_principal WHERE b_del = 0");

                    foreach ($banners as $banner) {
                        echo "<tr>";
                        echo "<td><img src=\"".wp_get_attachment_image_src($banner->imagem, "full")[0]."\"/></td>";
                        echo "<td>{$banner->link}</td>";
                        echo "<td>{$banner->posicao}</td>";
                        echo "<td><a class=\"edt_album\" href=\"{$edit_url}&id={$banner->id}\" >Editar</a></td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <script>
                jQuery(document).ready(function($) {
                    $('#table_imagens').DataTable();
                });
            </script>

            <br><br><input type="submit" value="Adicionar" class="button button-primary" />
        </form>
    </div>

<?php

} else {

    // Formulário de edição

    ?>

    <div class="wrap">
        <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
        <h2 class="title">Edite os dados e clique no botão de atualizar a seguir</h2>

        <?php

        $banners = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}banner_principal WHERE id = {$_GET["id"]} AND b_del = 0");
        foreach ($banners as $banner) {

            ?>
            <form method="post" id="form_edit_banners" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?=$banner->id?>" required/>
                <table class="form-table">
                    <tr>
                        <th>Texto </th>
                        <td><textarea name="texto"><?=$banner->texto?></textarea>
                        <p class="description">Texto opcional de destaque que fica sobrescrito em relação à imagem</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Link</th>
                        <td><input type="text" name="link" id="link" required value="<?=$banner->link?>"/>
                            <p class="description">Escreva aqui o link completo para onde o usuário deve ser redirecionado ao clicar no banner</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Prioridade</th>
                        <td><input type="number" name="ordem" id="ordem" required value="<?=$banner->ordem?>"/>
                            <p class="description">Digite um número de 1 a 10 que represente a prioridade na ordem de exibição desse banner. Sendo 1 a prioridade e 10 o banner que deve aparecer em último lugar.</p>
                        </td>
                    </tr>
                    <tr>
                        <th>Selecione uma das existentes na tabela abaixo abaixo ou faça o upload da fotos necessária no botão a seguir e depois retorne aqui para adicioná-las ao banner criado: </th>
                        <td><?= media_upload_form() ?></td>
                    </tr>
                </table>
                <input type="hidden" id="input_imagem_banner" name="input_imagem_banner" value="<?=$banner->imagem?>" required />
                <h2 class="title">Lista de imagens</h2>
                <table id="table_imagens" class="display">
                    <thead>
                        <tr>
                            <th>Imagens disponíveis</th>
                            <th>Nome do arquivo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $images = $wpdb->get_results("SELECT ID, post_name, `guid`, post_type, post_mime_type FROM wp_posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image%' ");
                        foreach ($images as $image) {
                            $check_banner_image = $banner->imagem == $image->ID ? "checked" : "";
                            echo "<tr>";
                            echo "<td><input type=\"radio\" class=\"imagem_banner\" name=\"imagem_banner\" value=\"{$image->ID}\" id=\"img_{$image->post_name}\" $check_banner_image/><label for=\"img_{$image->post_name}\"><img src=\"{$image->guid}\"/></label></td>";
                            echo "<td>{$image->post_name}</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>

                <script>
                    jQuery(document).ready(function($) {
                        $('#table_imagens').DataTable();
                        $("select[name=posicao]").val("<?=$banner->posicao?>")
                    });
                </script>

                <br><br><input type="submit" value="Atualizar" class="button button-primary" />
            </form>
        </div>

    <?php
}
}

?>