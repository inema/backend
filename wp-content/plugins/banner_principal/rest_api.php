<?php

// autoload agora está sendo chamado globalmente no arquivo cpt_altos_papos.php
// require_once 'vendor/autoload.php';
use ColorThief\ColorThief;

// API WP
add_action('rest_api_init', function () {
  register_rest_route('banner_principal/v1', '/banner_principal', array(
    'methods' => 'GET',
    'callback' => 'get_banner_principal',
  ));
});

function get_banner_principal($data)
{
  global $wpdb;
  $array_retorno = array();


  $banners = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}banner_principal WHERE b_del = 0 ORDER BY `ordem` ASC");

  foreach ($banners as $banner) {

    $palette = ColorThief::getPalette(wp_get_attachment_image_src($banner->imagem, "full")[0], 8);
  
    array_push( $array_retorno, array(
      "href" => $banner->link,
      "texto" => $banner->texto,
      "url" => wp_get_attachment_image_src($banner->imagem, "full")[0],
      "original" => wp_get_attachment_image_src($banner->imagem, "full")[0],
      "thumbnail" => wp_get_attachment_image_src($banner->imagem, "thumbnail")[0],
      "palette" => $palette
    ));
  }

  return $array_retorno;
}
