<?php

$current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$edit_url = get_admin_url(null, '', 'admin') . "admin.php?page=banner_principal-editar";
global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'excluir-processar.php');

$userID = get_current_user_id();

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1><a href="<?php echo get_admin_url(null, '', 'admin') ?>admin.php?page=banner_principal-criar" class="page-title-action">
        Criar novo</a><br />
    <p>Essas são os álbuns existentes, para editar ou excluir favor acessar na tabela a coluna de opções:</p>

    <form method="post" id="form_add_galerias" enctype="multipart/form-data">

        <table id="table_imagens" class="display">
            <thead>
                <tr>
                    <th>Banner</th>
                    <th>Link de redirecionamento</th>
                    <th>Prioridade de exibição</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $banner_principal = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}banner_principal WHERE b_del = 0 ORDER BY ordem ASC");

                foreach ($banner_principal as $banner) {
                    echo "<tr>";
                    echo "<td><img src=\"".wp_get_attachment_image_src($banner->imagem, "full")[0]."\"/></td>";
                    echo "<td>{$banner->link}</td>";
                    echo "<td>{$banner->ordem}</td>";
                    echo "<td><a class=\"del_album\" href=\"{$current_url}&id={$banner->id}\">Excluir</a> <a class=\"edt_album\" href=\"{$edit_url}&id={$banner->id}\" >Editar</a></td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function($) {
                $('#table_imagens').DataTable();
            });
        </script>

        <br><br><input type="submit" value="Adicionar" class="button button-primary" />
    </form>
</div>