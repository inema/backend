<?php

// API WP
add_action('rest_api_init', function () {
  register_rest_route('banners/v1', '/banners', array(
    'methods' => 'GET',
    'callback' => 'get_banners',
  ));
});

function get_banners($data)
{
  global $wpdb;
  $array_retorno = array();

  $banners_superiores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}banners WHERE posicao = 'superior' AND b_del = 0 ORDER BY `datetime` DESC");

  $array_banners_superiores = array();
  foreach ($banners_superiores as $banner) {
    array_push( $array_banners_superiores, array(
      "href" => $banner->link,
      "url" => wp_get_attachment_image_src($banner->imagem, "full")[0],
      "original" => wp_get_attachment_image_src($banner->imagem, "full")[0],
      "thumbnail" => wp_get_attachment_image_src($banner->imagem, "thumbnail")[0]
    ));
  }

  $banners_inferiores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}banners WHERE posicao = 'inferior' AND b_del = 0 ORDER BY `datetime` DESC");

  $array_banners_inferiores = array();
  foreach ($banners_inferiores as $banner) {
    array_push( $array_banners_inferiores, array(
      "href" => $banner->link,
      "url" => wp_get_attachment_image_src($banner->imagem, "full")[0],
      "original" => wp_get_attachment_image_src($banner->imagem, "full")[0],
      "thumbnail" => wp_get_attachment_image_src($banner->imagem, "thumbnail")[0]
    ));
  }

  array_push( $array_retorno, $array_banners_superiores );
  array_push( $array_retorno, $array_banners_inferiores );

  return $array_retorno;
}
