<?php

// Roles AND capabilities
function wporg_banners_role()
{
    add_role(
        'editor_banners',
        'Editor de Banners',
        [
            'read'         => true,
            'edit_posts'   => true,
            'upload_files' => true,
        ]
    );
}
add_action('init', 'wporg_banners_role', 10);

function wporg_banners_role_caps()
{
    // gets the simple_role role object
    $role = get_role('editor_banners');
 
    // add a new capability
    $role->add_cap('ascom_banners', true);
}
 
// add simple_role capabilities, priority must be after the initial role definition
add_action('init', 'wporg_banners_role_caps', 11);

?>