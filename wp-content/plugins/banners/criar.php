<?php

global $wpdb;

//  Incluindo página de processamento para dar POST em banco de dados
include_once(plugin_dir_path(__FILE__) . 'criar-processar.php');

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
    <h2 class="title">Para adicionar nova galeria preencha o formulário a seguir</h2>
    <form method="post" id="form_ad_banner" enctype="multipart/form-data">
        <table class="form-table">
            <tr>
                <th>Localização</th>
                <td><select name="posicao">
                        <option value="superior" selected>Superior</option>
                        <option value="inferior">Inferior</option>
                    </select>
                    <p class="description">Escolha se esse banner fará parte do carrousel superior ou inferior</p>
                </td>
            </tr>
            <tr>
                <th>Link</th>
                <td><input type="text" name="link" id="link" required />
                    <p class="description">Escreva aqui o link completo para onde o usuário deve ser redirecionado ao clicar no banner</p>
                </td>
            </tr>
            <tr>
                <th>Selecione uma das existentes na tabela abaixo abaixo ou faça o upload da fotos necessária no botão a seguir e depois retorne aqui para adicioná-las ao banner criado: </th>
                <td><?= media_upload_form() ?></td>
            </tr>
        </table>
        <input type="hidden" id="input_imagem_banner" name="input_imagem_banner" required />
        <h2 class="title">Lista de imagens</h2>
        <p>Estão sendo exibidas apenas imagens com 275px x 105px ou de proporção semelhante</p>
        <table id="table_imagens" class="display">
            <thead>
                <tr>
                    <th>Imagens disponíveis</th>
                    <th>Nome do arquivo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $images = $wpdb->get_results("SELECT ID, post_name, `guid`, post_type, post_mime_type FROM wp_posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image%' ");
                foreach ($images as $image) {
                    $width = wp_get_attachment_metadata($image->ID)["width"];
                    $height = wp_get_attachment_metadata($image->ID)["height"];
                    $ratio = $width/$height;
                    if($ratio > 2.6 && $ratio < 2.63){
                        echo "<tr>";
                        echo "<td><input type=\"radio\" class=\"imagem_banner\" name=\"imagem_banner\" value=\"{$image->ID}\" id=\"img_{$image->post_name}\"/><label for=\"img_{$image->post_name}\"><img src=\"{$image->guid}\"/></label></td>";
                        echo "<td>{$image->post_name}</td>";
                        echo "</tr>";
                    }
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function($) {
                $('#table_imagens').DataTable();
            });
        </script>

        <br><br><input type="submit" value="Adicionar" class="button button-primary" />
    </form>
</div>