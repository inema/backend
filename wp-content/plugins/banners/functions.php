<?php

// Adicionar scripts para rodar no dashboard
$hook_list_banners = array(
    "toplevel_page_banners",
    "banners_page_banners-editar",
    "banners_page_banners-criar"
);

function plugin_banners_add_scripts_0125($hook) {
    // print_r($hook); // toplevel_page_banners
    global $hook_list_banners;
    if( !in_array($hook, $hook_list_banners) ) return;
    wp_enqueue_script('plugin_banners_datatables', plugins_url('/js/DataTables/datatables.js', __FILE__), array('jquery'), '1.0', true);
    wp_enqueue_script('plugin_banners_main_script', plugins_url('/js/main.js', __FILE__), array('jquery'), '1.0', true);
}

add_action('admin_enqueue_scripts', 'plugin_banners_add_scripts_0125');

function plugin_banners_add_style_2354($hook) {
    global $hook_list_banners;
    if( !in_array($hook, $hook_list_banners) ) return;
    wp_enqueue_style('plugin_banners_datatables', plugins_url('/css/DataTables/datatables.css', __FILE__));
    wp_enqueue_style('plugin_banners_main_style', plugins_url('/css/main.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'plugin_banners_add_style_2354' );

?>