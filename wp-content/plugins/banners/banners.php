<?php

/*
Plugin Name: Banners
Plugin URI: https://wordpress.com
Description: Um plugin criado para gerenciar os banners laterais da intranet INEMA
Version: 0.1
Author: Filipe Lopes
Author URI: https://filipelopes.me
License: CC BY-NC 3.0 BR
Text Domain: banners
*/

// Functions
include_once( plugin_dir_path( __FILE__ ) . 'functions.php' );

// API WP
include_once( plugin_dir_path( __FILE__ ) . 'rest_api.php' );

// Roles and capabilities for plugin
include_once( plugin_dir_path( __FILE__ ) . 'roles_cap.php' );

// Ação de executar o admin_menu no core do WORDPRESS 
add_action('admin_menu', 'menu_banners');

function menu_banners(){
    global $wpdb;
    global $wp_roles;

    $menu_slug = "banners";
    $capability = "ascom_banners";

    // Adiciona menu na barra lateral
    
    /* add_menu_page( 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '', 
        string $icon_url = '', 
        int $position = null ) */
    add_menu_page( 
        'Banners laterais', 
        'Banners', 
        $capability , 
        $menu_slug , 
        'mostrar_banners' , 
        'dashicons-images-alt' , 
        20 );

    /* add_submenu_page( 
        string $parent_slug, 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '' ) */
    add_submenu_page( 
        $menu_slug , 
        'Inserir novo banner', 
        'Adicionar novo', 
        $capability , 
        'banners-criar', 
        'adicionar_banner' );

    add_submenu_page( 
        $menu_slug , 
        'Editar banner existente', 
        'Editar Banner', 
        $capability, 
        'banners-editar', 
        'editar_banner' );
    
    // Cria tabela que será usada no plugin
    $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}banners` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `link` varchar(200) DEFAULT NULL,
        `imagem` int(11) DEFAULT NULL,
        `posicao` varchar(20) NOT NULL DEFAULT 'superior',
        `usuario` int(11) unsigned NOT NULL,
        `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,        
        `b_del` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `usuario` (`usuario`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8, AUTO_INCREMENT=1";
    
    $wpdb->query($sql);
}

function mostrar_banners(){
    include('pagina_principal.php');
}

function adicionar_banner(){
    include('criar.php');
}

function editar_banner(){
    include('editar.php');
}
