<?php

if(isset($_POST["nome"])):
global $wpdb;

// $link = $_POST["link"];
// $imagem = $_POST["input_imagem_banner"];
// $posicao = $_POST["posicao"];
// $usuario = get_current_user_id();
// $datetime = date("Y-m-d H:i:s");

$nome = $_POST["nome"];
$mostrar_foto = isset($_POST["mostrar_foto"]) ? 1 : 0;
$avatar = $_POST["avatar"];
$data_nascimento = $_POST["data_nascimento"];
$mostrar_aniversario = isset($_POST["mostrar_aniversario"]) ? 1 : 0 ;
$data_admissao = $_POST["data_admissao"];
$orgao = $_POST["orgao"];
$setor = $_POST["setor"];
$biografia = $_POST["biografia"];
$especialidades = $_POST["especialidades"];
$atividades = $_POST["atividades"];
$usuario = get_current_user_id();

// print_r($_FILES["foto"]);

if($_FILES["foto"]['size']){
    $errors= array();
    $file_name = $_FILES['foto']['name'];
    $file_size =$_FILES['foto']['size'];
    $file_tmp =$_FILES['foto']['tmp_name'];
    $file_type=$_FILES['foto']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['foto']['name'])));

    $extensions= array("jpeg","jpg","png");

    if(in_array($file_ext,$extensions)=== false){
        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }

    if($file_size > (2097152/2)*10){
        $errors[]='File size must be excately 10 MB';
    }
    echo "UPLOADS_DIR  '{$wp_uploads_dir}'";
    if(empty($errors)==true){
        move_uploaded_file($file_tmp, $wp_uploads_dir."/colaboradores/".$file_name);
        echo "Success";
    }else{
        echo "Errors:";
        print_r($errors);
        echo "\n";
    }

    // Primeiro adiciona imagem
    $wpdb->query(" 
        INSERT INTO `{$wpdb->prefix}colaboradores_arquivos` (
        `id` ,
        `nome` ,
        `type` ,
        `path` ,
        `mime_type`
        )
        VALUES (
        NULL ,  '$file_name',  'foto', '$file_name', '$file_type'
        );")or die($wpdb->error);

    $id_imagem = $wpdb->insert_id;
}else{
    $id_imagem = $avatar;
}

// Depois adiciona colaborador
$wpdb->query(" 
    INSERT INTO `{$wpdb->prefix}colaboradores` (
    `id` ,
    `nome`,
    `foto`,
    `mostrar_foto`,
    `data_nascimento`,
    `mostrar_aniversario`,
    `data_admissao`,
    `orgao`,
    `setor`,
    `biografia`,
    `especialidades`,
    `atividades`,
    `usuario`
    )
    VALUES (
    NULL ,  '$nome',  '$id_imagem', '$mostrar_foto', '$data_nascimento',  '$mostrar_aniversario', '$data_admissao', '$orgao', '$setor', '$biografia', '$especialidades', '$atividades', '$usuario'
    );")or die($wpdb->error);

echo "<span class='success'>Colaborador adicionado com sucesso</span>";

endif;

?>