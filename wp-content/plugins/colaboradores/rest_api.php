<?php

$wp_uploads_url = wp_get_upload_dir()["baseurl"];

/* ----------
  API REST para GET de colaboradores
---------- */

add_action('rest_api_init', function () {
  register_rest_route('colaboradores/v1', '/colaboradores(?:/(?P<id>[\d]+))?', array(
    'methods' => 'GET',
    'callback' => 'get_colaboradores',
  ));
});

function get_colaboradores($data)
{
  global $wpdb;
  global $wp_uploads_url;

  $retorno = array();
  if(isset($data['id'])){
    $colaboradores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores WHERE id = {$data['id']} ORDER BY `data_admissao` DESC");
  }else{
    $colaboradores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores WHERE b_del = 0 ORDER BY `data_admissao` DESC");
  }

  foreach ($colaboradores as $colaborador) {
    $foto_id = intval($colaborador->foto);
    $imagem = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE id = {$foto_id}");

    $imagem->path = $wp_uploads_url."/colaboradores/".$imagem->path;

    $setor_id = $colaborador->setor;
    $setor = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_setores WHERE id = {$setor_id}");

    $colaborador->foto = $imagem;
    $colaborador->setor = $setor->setor;
    array_push($retorno, $colaborador);
  }

  if(isset($data['id'])){
    return $retorno[0];
  }else{
    return $retorno;
  }
  
}

/* ----------
  API REST para GET de novos colaboradores
---------- */

add_action('rest_api_init', function () {
  register_rest_route('colaboradores/v1', '/novos-colaboradores', array(
    'methods' => 'GET',
    'callback' => 'get_novos_colaboradores',
  ));
});

function get_novos_colaboradores($data)
{
  global $wpdb;
  global $wp_uploads_url;
  
  $retorno = array();
  $colaboradores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores WHERE data_admissao > DATE_SUB(CURDATE(), INTERVAL 3 MONTH)");


  foreach ($colaboradores as $colaborador) {
    $foto_id = intval($colaborador->foto);
    $imagem = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE id = {$foto_id}");

    $imagem->path = $wp_uploads_url."/colaboradores/".$imagem->path;

    $setor_id = $colaborador->setor;
    $setor = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_setores WHERE id = {$setor_id}");

    $colaborador->foto = $imagem;
    $colaborador->setor = $setor->setor;
    array_push($retorno, $colaborador);
  }

  return $retorno;
}

/* ----------
  API REST para GET de Aniversariantes do dia
---------- */

add_action('rest_api_init', function () {
  register_rest_route('colaboradores/v1', '/aniversariantes/(?P<mes>[\d]+)/(?P<dia>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'get_aniversariantes',
  ));
});

function get_aniversariantes($data)
{
  global $wpdb;
  global $wp_uploads_url;

  $mes = intval($data["mes"]);
  $dia = intval($data["dia"]);

  // Capturando apenas colaboradores que fazem aniversario no dia solicitado e que gostariam que seu aniversario fosse exibido ao público
  $colaboradores = $wpdb->get_results("SELECT * FROM wp_colaboradores WHERE MONTH(data_nascimento) = $mes AND DAY(data_nascimento) = $dia AND mostrar_aniversario = 1 AND b_del = 0 ORDER BY `data_admissao` DESC");
      
  foreach ($colaboradores as $colaborador) {
    $foto_id = intval($colaborador->foto);
    $imagem = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE id = {$foto_id}");
    
    $imagem->path = $wp_uploads_url."/colaboradores/".$imagem->path;
    
    $setor_id = $colaborador->setor;
    $setor = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_setores WHERE id = {$setor_id}");
    
    $colaborador->foto = $imagem;
    $colaborador->setor = $setor->setor;
    array_push($retorno, $colaborador);
  }
  
  return $colaboradores;

}