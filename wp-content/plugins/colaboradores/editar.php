<?php
$current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
global $wpdb;

$userID = get_current_user_id();
$wp_uploads_colaboradores_url = wp_get_upload_dir()["baseurl"]."/colaboradores/";
$edit_url=get_admin_url( null, '', 'admin' )."admin.php?page=colaboradores-editar";

include_once(plugin_dir_path(__FILE__) . 'editar-processar.php');

if(!isset($_GET["id"])){
    
?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1><a href="<?php echo get_admin_url(null, '', 'admin') ?>admin.php?page=colaboradores-criar" class="page-title-action">
        Criar novo</a><br />
    <p>Essas são os álbuns existentes, para editar ou excluir favor acessar na tabela a coluna de opções:</p>

    <form method="post" id="form_add_galerias" enctype="multipart/form-data">

        <table id="table_imagens" class="display">
            <thead>
                <tr>
                    <th>Banner</th>
                    <th>Nome</th>
                    <th>Data de nascimento</th>
                    <th>Data de admissão</th>
                    <th>Órgão</th>
                    <th>Setor</th>
                    <th>Mais</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $colaboradores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores WHERE b_del = 0");
                
                foreach ($colaboradores as $colaborador) {
                    $image_perfil = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE id = $colaborador->foto");
                    $mostrar_foto = $colaborador->mostrar_foto == 1 ? "green" : "red";
                    $mostrar_aniversario = $colaborador->mostrar_aniversario == 1 ? "green" : "red";
                    echo "<tr>";
                    echo "<td class=\"foto_perfil\" style=\"background-image: url('{$wp_uploads_colaboradores_url}{$image_perfil->path}')\"><div class=\"cover $mostrar_foto\"></div></td>";
                    echo "<td>{$colaborador->nome}</td>";
                    echo "<td><div class=\"cover $mostrar_aniversario\" ></div>{$colaborador->data_nascimento}</td>";
                    echo "<td>{$colaborador->data_admissao}</td>";
                    echo "<td>{$colaborador->orgao}</td>";
                    echo "<td>{$colaborador->setor}</td>";
                    echo "<td><span class=\"more\"> <a class=\"ver_mais\">Ver mais</a>
                    <div>
                    <p> <strong>Biografia:</strong> {$colaborador->biografia} </p>
                    <p> <strong>Especialidades:</strong> {$colaborador->especialidades} </p>
                    <p> <strong>Atividades:</strong> {$colaborador->atividades} </p>
                    </div>
                    </span></td>";
                    echo "<td><a class=\"edt_album\" href=\"{$edit_url}&id={$colaborador->id}\" >Editar</a></td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function($) {
                $('#table_imagens').DataTable();
            });
        </script>

        <br><br><input type="submit" value="Adicionar" class="button button-primary" />
    </form>
</div>

<?php 

}else{

// Formulário de edição

?>

<?php
    $colaborador = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores WHERE id = {$_GET["id"]}");

    $imagem_perfil = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE id = {$colaborador->foto}");

    $hasPhoto = $imagem_perfil->type == "foto" ? 1 : 0;
    $isAvatar = $imagem_perfil->type == "avatar" ? 1 : 0;
    $mostrar_aniversario = $colaborador->mostrar_aniversario == 1 ? "checked" : ""; 
    $mostrar_foto = $colaborador->mostrar_foto == 1 ? "checked" : ""; 
?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
    <h2 class="title">Para adicionar novo colaborador preencha o formulário a seguir</h2>
    <?= $plugin_dir ?>
    <form method="post" id="form_colaboradores_editar" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?=$_GET["id"]?>"/>
        <table class="form-table">
            <tr>
                <th>Nome</th>
                <td><input type="text" name="nome" value="<?=$colaborador->nome?>" required />
                </td>
            </tr>
            <tr>
                <th>Foto</th>
                <td>
                    <div class="excluir_foto_box" style="background-image: url('<?=$wp_uploads_colaboradores_url?><?=$imagem_perfil->nome?>')"><div class="cover"></div><button type="button" id="excluir_foto">Excluir foto</button></div>
                    <div class="nova_foto_box"><input type="file" name="foto" id="foto"/>
                    <p class="description">Faça o upload da foto do colaborador ou selecione um dos avatares abaixo</p></div>
                    <input type="hidden" name="foto_hid" value="<?=$hasPhoto?>"/>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <div class="checkbox_advanced_label avatares_box" style="display:none">
                        <?php
                        $avatares = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE type = 'avatar' ");
                        foreach ($avatares as $avatar) {
                            echo "
                            <div>
                            <input name=\"avatar\" type=\"radio\" value=\"{$avatar->id}\" id=\"avatar_{$avatar->id}\"/>
                            <label for=\"avatar_{$avatar->id}\"> <div style=\"background-image: url('{$wp_uploads_colaboradores_url}{$avatar->path}')\" ></div> </label>
                            </div>";
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th>Mostrar foto</th>
                <td><input type="checkbox" name="mostrar_foto" <?=$mostrar_foto?> /></td>
            </tr>
            <tr>
                <th>Data de nascimento</th>
                <td><input type="date" name="data_nascimento" value="<?=$colaborador->data_nascimento?>" />
                </td>
            </tr>
            <tr>
                <th>Mostrar aniversário</th>
                <td><input type="checkbox" name="mostrar_aniversario" <?=$mostrar_aniversario?> /></td>
            </tr>
            <tr>
                <th>Data de admissão</th>
                <td><input type="date" name="data_admissao" value="<?=$colaborador->data_admissao?>" />
                </td>
            </tr>
            <tr>
                <th>Órgão</th>
                <td><select name="orgao" >
                        <option value="">Selecione um órgão</option>
                        <option value="INEMA">INEMA</option>
                        <option value="SEMA">SEMA</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Setor</th>
                <td><select name="setor" >
                        <option value="">Selecione um setor</option>
                        <?php
                        $setores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores_setores");
                        foreach ($setores as $setor) {
                            echo "<option value='{$setor->id}'>{$setor->setor}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Biografia</th>
                <td>
                    <?php wp_editor( $colaborador->biografia , "biografia", array(
                        "media_buttons" => false
                    )); ?>
                </td>
            </tr>
            <tr>
                <th>Especialidades</th>
                <td>
                    <input type="text" name="especialidades" value="<?=$colaborador->especialidades?>"/>
                    <p class="description">Digite as especialidades separadas por vírgulas</p>
                </td>
            </tr>
            <tr>
                <th>Atividades</th>
                <td>
                    <input type="text" name="atividades" value="<?=$colaborador->atividades?>"/>
                    <p class="description">Digite as atividades separadas por vírgulas</p>
                </td>
            </tr>
        </table>

        <br><br><input type="submit" value="Atualizar" class="button button-primary" />
    </form>
</div>

<script>
    // Adiciono valor ao select pelo JQuery
jQuery(document).ready( function($) {
    isAvatar = <?=$isAvatar?>;
    $("select[name=orgao]").val("<?=$colaborador->orgao?>")
    $("select[name=setor]").val("<?=$colaborador->setor?>")

    if(isAvatar){
        $("div.excluir_foto_box").hide()
        $("div.nova_foto_box").show()
        $("div.avatares_box").show()
    }
})
</script>

<?php

}

?>