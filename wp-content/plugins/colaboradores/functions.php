<?php

// Adicionar scripts para rodar no dashboard
$hook_list_colaboradores = array(
    "toplevel_page_colaboradores",
    "colaboradores_page_colaboradores-criar",
    "colaboradores_page_colaboradores-editar"
);

function plugin_colaboradores_add_scripts_0125($hook) {
    global $hook_list_colaboradores;
    if( !in_array($hook, $hook_list_colaboradores) ) return;
    wp_enqueue_script('plugin_colaboradores_datatables', plugins_url('/js/DataTables/datatables.js', __FILE__), array('jquery'), '1.0', true);
    wp_enqueue_script('plugin_colaboradores_main_script', plugins_url('/js/main.js', __FILE__), array('jquery'), '1.0', true);
}
add_action('admin_enqueue_scripts', 'plugin_colaboradores_add_scripts_0125');

function plugin_colaboradores_add_style_2354($hook) {
    global $hook_list_colaboradores;
    if( !in_array($hook, $hook_list_colaboradores) ) return;
    wp_enqueue_style('plugin_colaboradores_datatables', plugins_url('/css/DataTables/datatables.css', __FILE__));
    wp_enqueue_style('plugin_colaboradores_main_style', plugins_url('/css/main.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'plugin_colaboradores_add_style_2354' );

?>