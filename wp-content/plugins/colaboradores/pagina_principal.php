<?php
$current_url = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'excluir-processar.php');

$userID = get_current_user_id();
$wp_uploads_colaboradores_url = wp_get_upload_dir()["baseurl"]."/colaboradores/";
$edit_url=get_admin_url( null, '', 'admin' )."admin.php?page=colaboradores-editar";

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1><a href="<?php echo get_admin_url(null, '', 'admin') ?>admin.php?page=colaboradores-criar" class="page-title-action">
        Criar novo</a><br />
    <p>Essas são os colaboradores existentes, para editar ou excluir favor acessar na tabela a coluna de opções:</p>

    <form method="post" id="form_add_galerias" enctype="multipart/form-data">

        <table id="table_imagens" class="display">
            <thead>
                <tr>
                    <th>Banner</th>
                    <th>Nome</th>
                    <th>Data de nascimento</th>
                    <th>Data de admissão</th>
                    <th>Órgão</th>
                    <th>Setor</th>
                    <th>Mais</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $setores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores_setores");

                $colaboradores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores WHERE b_del = 0");

                foreach ($colaboradores as $colaborador) {
                    $image_perfil = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE id = $colaborador->foto");
                    $mostrar_foto = $colaborador->mostrar_foto == 1 ? "green" : "red";
                    $mostrar_aniversario = $colaborador->mostrar_aniversario == 1 ? "green" : "red";

                    // Substituir propriedade de id por nome do setor
                    foreach($setores as $setor){
                        if($setor->id == $colaborador->setor){
                            $colaborador->setor = $setor->setor;
                        }
                    }

                    echo "<tr>";
                    echo "<td class=\"foto_perfil\" style=\"background-image: url('{$wp_uploads_colaboradores_url}{$image_perfil->path}')\"><div class=\"cover $mostrar_foto\"></div></td>";
                    echo "<td>{$colaborador->nome}</td>";
                    echo "<td><div class=\"cover $mostrar_aniversario\" ></div>{$colaborador->data_nascimento}</td>";
                    echo "<td>{$colaborador->data_admissao}</td>";
                    echo "<td>{$colaborador->orgao}</td>";
                    echo "<td>{$colaborador->setor}</td>";
                    echo "<td><span class=\"more\"> <a class=\"ver_mais\">Ver mais</a>
                    <div>
                    <p> <strong>Biografia:</strong> {$colaborador->biografia} </p>
                    <p> <strong>Especialidades:</strong> {$colaborador->especialidades} </p>
                    <p> <strong>Atividades:</strong> {$colaborador->atividades} </p>
                    </div>
                    </span></td>";
                    echo "<td><a class=\"del_album\" href=\"{$current_url}&id={$colaborador->id}\">Excluir</a> <a class=\"edt_album\" href=\"{$edit_url}&id={$colaborador->id}\" >Editar</a></td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
        <script>
            jQuery(document).ready(function($) {
                $('#table_imagens').DataTable();
            });
        </script>

        <br><br><input type="submit" value="Adicionar" class="button button-primary" />
    </form>
</div>