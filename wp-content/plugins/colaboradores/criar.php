<?php

global $wpdb;


// Diretório do plugin
$plugin_dir = plugin_dir_url(__FILE__);
$upload_folder = $plugin_dir . "";

$wp_uploads_url = wp_get_upload_dir()["baseurl"];
$wp_uploads_dir = wp_get_upload_dir()["basedir"];

//  Incluindo página de processamento para dar POST em banco de dados
include_once(plugin_dir_path(__FILE__) . 'criar-processar.php');

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
    <h2 class="title">Para adicionar novo colaborador preencha o formulário a seguir</h2>
    <?= $plugin_dir ?>
    <form method="post" id="form_ad_banner" enctype="multipart/form-data">
        <table class="form-table">
            <tr>
                <th>Nome</th>
                <td><input type="text" name="nome" required />
                </td>
            </tr>
            <tr>
                <th>Foto</th>
                <td><input type="file" name="foto" id="foto"/>
                    <p class="description">Faça o upload da foto do colaborador ou selecione um dos avatares abaixo</p>
                </td>
            </tr>
            <tr>
                <th>Mostrar foto</th>
                <td><input type="checkbox" name="mostrar_foto" checked /></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <div class="checkbox_advanced_label">
                        <?php
                        $avatares = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores_arquivos WHERE type = 'avatar' ");
                        foreach ($avatares as $avatar) {
                            echo "
                            <div>
                            <input name=\"avatar\" type=\"radio\" value=\"{$avatar->id}\" id=\"avatar_{$avatar->id}\"/>
                            <label for=\"avatar_{$avatar->id}\"> <div style=\"background-image: url('{$wp_uploads_url}/colaboradores/{$avatar->path}')\" ></div> </label>
                            </div>";
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th>Data de nascimento</th>
                <td><input type="date" name="data_nascimento" />
                </td>
            </tr>
            <tr>
                <th>Mostrar aniversário</th>
                <td><input type="checkbox" name="mostrar_aniversario" checked /></td>
            </tr>
            <tr>
                <th>Data de admissão</th>
                <td><input type="date" name="data_admissao"  />
                </td>
            </tr>
            <tr>
                <th>Órgão</th>
                <td><select name="orgao" >
                        <option value="">Selecione um órgão</option>
                        <option value="INEMA">INEMA</option>
                        <option value="SEMA">SEMA</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Setor</th>
                <td><select name="setor" >
                        <option value="">Selecione um setor</option>
                        <?php
                        $setores = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}colaboradores_setores");
                        foreach ($setores as $setor) {
                            echo "<option value='{$setor->id}'>{$setor->setor}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Biografia</th>
                <td>
                    <?php wp_editor("", "biografia", array(
                        "media_buttons" => false
                    )); ?>
                </td>
            </tr>
            <tr>
                <th>Especialidades</th>
                <td>
                    <input type="text" name="especialidades" />
                    <p class="description">Digite as especialidades separadas por vírgulas</p>
                </td>
            </tr>
            <tr>
                <th>Atividades</th>
                <td>
                    <input type="text" name="atividades" />
                    <p class="description">Digite as atividades separadas por vírgulas</p>
                </td>
            </tr>
        </table>

        <br><br><input type="submit" value="Adicionar" class="button button-primary" />
    </form>
</div>