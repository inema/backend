<?php

/*
Plugin Name: Colaboradores
Plugin URI: https://wordpress.com
Description: Um plugin criado para gerenciar os colaboradores/funcionários do INEMA/SEMA
Version: 0.1
Author: Filipe Lopes
Author URI: https://filipelopes.me
License: CC BY-NC 3.0 BR
Text Domain: banners
*/

// Functions
include_once( plugin_dir_path( __FILE__ ) . 'functions.php' );

// API WP
include_once( plugin_dir_path( __FILE__ ) . 'rest_api.php' );

// Roles and capabilities for plugin
include_once( plugin_dir_path( __FILE__ ) . 'roles_cap.php' );

// Ação de executar o admin_menu no core do WORDPRESS 
add_action('admin_menu', 'menu_colaboradores');

function menu_colaboradores(){
    global $wpdb;
    global $wp_roles;

    $menu_slug = "colaboradores";
    $capability = "ascom_colaboradores";

    // Adiciona menu na barra lateral
    
    /* add_menu_page( 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '', 
        string $icon_url = '', 
        int $position = null ) */
    add_menu_page( 
        'Gerenciador de Colaboradores', 
        'Colaboradores', 
        $capability , 
        $menu_slug , 
        'mostrar_colaboradores' , 
        'dashicons-id-alt' , 
        20 );

    /* add_submenu_page( 
        string $parent_slug, 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '' ) */
    add_submenu_page( 
        $menu_slug , 
        'Inserir novo colaborador', 
        'Adicionar novo colaborador', 
        $capability , 
        'colaboradores-criar', 
        'adicionar_colaborador' );

    add_submenu_page( 
        $menu_slug , 
        'Editar colaborador existente', 
        'Editar Colaborador', 
        $capability, 
        'colaboradores-editar', 
        'editar_colaborador' );
    
    // Cria tabela que será usada no plugin
    $sql = "CREATE TABLE `{$wpdb->prefix}colaboradores` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `nome` varchar(50) NOT NULL,
        `foto` int(11) DEFAULT NULL,
        `mostrar_foto` tinyint(1) NOT NULL,
        `data_nascimento` date DEFAULT NULL,
        `mostrar_aniversario` tinyint(1) NOT NULL,
        `data_admissao` date DEFAULT NULL,
        `orgao` varchar(20) DEFAULT NULL,
        `setor` int(11) DEFAULT NULL,
        `biografia` text,
        `especialidades` text,
        `atividades` text,
        `usuario` int(11) unsigned NOT NULL,
        `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `b_del` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `usuario` (`usuario`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8, AUTO_INCREMENT=1";
    
    $wpdb->query($sql);
}

function mostrar_colaboradores(){
    include('pagina_principal.php');
}

function adicionar_colaborador(){
    include('criar.php');
}

function editar_colaborador(){
    include('editar.php');
}
