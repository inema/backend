<?php

// API WP
add_action('rest_api_init', function () {
  register_rest_route('galerias/v1', '/albuns', array(
    'methods' => 'GET',
    'callback' => 'my_awesome_func',
  ));

  register_rest_route('galerias/v1', '/albuns/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'get_unique_album',
  ));
});

function my_awesome_func($data)
{
  global $wpdb;
  $array_retorno = array();
  $albuns = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}galerias WHERE b_del = 0");

  foreach ($albuns as $album) {
    $array_imagens = array();
    $imagens = json_decode($album->imagens);
    // Imagens
    foreach ($imagens as $imagem) {
      array_push($array_imagens, array(
        "post_id" => $imagem,
        "url" => wp_get_attachment_image_src($imagem, "full")[0],
        "original" => wp_get_attachment_image_src($imagem, "full")[0],
        "thumbnail" => wp_get_attachment_image_src($imagem, "thumbnail")[0]
      ));
    }
    // Capa
    $album->imagens = $array_imagens;
    $album->capa = array(
      "post_id" => $album->capa,
      "url" => wp_get_attachment_image_src($album->capa, "full")[0],
      "original" => wp_get_attachment_image_src($album->capa, "full")[0],
      "thumbnail" => wp_get_attachment_image_src($album->capa, "thumbnail")[0]
    );
    array_push($array_retorno, $album);
  }

  return $array_retorno;
}

function get_unique_album($data)
{
  global $wpdb;
  $albuns = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}galerias WHERE id = {$data["id"]} AND b_del = 0");

  foreach ($albuns as $album) {
    $array_imagens = array();
    $imagens = json_decode($album->imagens);
    // Imagens
    foreach ($imagens as $imagem) {
      array_push($array_imagens, array(
        "post_id" => $imagem,
        "url" => wp_get_attachment_image_src($imagem, "full")[0],
        "original" => wp_get_attachment_image_src($imagem, "full")[0],
        "thumbnail" => wp_get_attachment_image_src($imagem, "thumbnail")[0]
      ));
    }
    // Capa
    $album->imagens = $array_imagens;
    $album->capa = array(
      "post_id" => $album->capa,
      "url" => wp_get_attachment_image_src($album->capa, "full")[0],
      "original" => wp_get_attachment_image_src($album->capa, "full")[0],
      "thumbnail" => wp_get_attachment_image_src($album->capa, "thumbnail")[0]
    );
  }

  return $album;
}
