<?php
$plugin_data = get_plugin_data( dirname( __FILE__ ) . '/galerias.php' );
$plugin_name = $plugin_data['Name'];
$edit_url=get_admin_url( null, '', 'admin' )."admin.php?page=galeria-editar";
global $wpdb;

//novo

include_once( plugin_dir_path( __FILE__ ) . 'editar-processar.php' );

if(!isset($_GET["id"])){
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>
    <h2 class="title">Selecione uma das galerias abaixo que deseja editar</h2>
    <form method="post" id="form_add_galerias" enctype="multipart/form-data">

    <table id="table_imagens" class="display">
    <thead>
        <tr>
            <th>Nome da galeria</th>
            <th>Imagem de capa</th>
            <th>Data de criação</th>
            <th>Opções</th>
        </tr>
    </thead>
    <tbody>
    <?php
            $albuns = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}galerias WHERE b_del = 0");

            foreach($albuns as $album){
                echo "<tr>";
                echo "<td><img src=\"".wp_get_attachment_image_src($album->capa, "full")[0]."\"/></td>";
                echo "<td>{$album->titulo}</td>";
                echo "<td>{$album->id}</td>";
                echo "<td> <a class=\"edt_album\" href=\"{$edit_url}&id={$album->id}\" >Editar</a></td>";
                echo "</tr>";
            }
        ?>
    </tbody>
</table>
<script>
jQuery(document).ready( function($){
    $('#table_imagens').DataTable();
}); 
</script>
        
        <br><br><input type="submit" value="Adicionar" class="button button-primary"/>
    </form>
</div>

<?php 

}else{

// Formulário de edição

?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>
    <h2 class="title">Edite os dados e clique no botão de atualizar a seguir</h2>

    <?php

    $albuns = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}galerias WHERE id = {$_GET["id"]} AND b_del = 0");
    foreach($albuns as $album){
    
    ?>
    <form method="post" id="form_add_galerias" enctype="multipart/form-data">
        <table class="form-table">
            <tr>
                <th>Título</th>
                <td><input type="text" maxlength="150" name="titulo" id="titulo" value="<?=$album->titulo?>" required/><p class="description">Deve ter, no mínimo, 6 letras</p></td>
            </tr>
        </table>
        <input type="hidden" id="id" name="id" value="<?=$_GET["id"]?>"/> 
        <input type="hidden" id="capa" name="capa" value="<?=$album->capa?>"/> 
        <textarea id="textarea_galeria_imagens" name="imagens" style="display:none" required><?=$album->imagens?></textarea>
        <h2 class="title">Lista de imagens</h2>
<table id="table_imagens" class="display">
    <thead>
        <tr>
            <th>Imagens</th>
            <th>Selecionar capa</th>
            <th>Nome do arquivo</th>
            <th>Pertencia ao álbum?</th>
        </tr>
    </thead>
    <tbody>
    <?php
        $album_images = json_decode($album->imagens);        
        $all_images = $wpdb->get_results("SELECT ID, post_name, `guid`, post_type, post_mime_type FROM wp_posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image%' ");
        foreach($all_images as $image){
            $is_album_image = in_array($image->ID, $album_images) ? "Sim" : "Não";
            $check_album_image = in_array($image->ID, $album_images) ? "checked" : "";
            echo "<tr>";
            echo "<td><input type=\"checkbox\" class=\"imagem_galeria\" name=\"imagem_galeria\" value=\"{$image->ID}\" id=\"img_{$image->post_name}\" $check_album_image/><label for=\"img_{$image->post_name}\"><img src=\"{$image->guid}\"/></label></td>";
            echo "<td><input type=\"radio\" name=\"cover\" value=\"{$image->ID}\"/></td>";
            echo "<td>{$image->post_name}</td>";
            echo "<td>$is_album_image</td>";
            echo "</tr>";
        }

        ?>
    </tbody>
</table>
<script>
jQuery(document).ready( function($){
    $('#table_imagens').DataTable();

    $("#table_imagens input[name=cover][value=<?=$album->capa?>]").attr("checked", true);
}); 
</script>
        
        <br><br><input type="submit" value="Atualizar" class="button button-primary"/>
    </form>
</div>

<?php
    }

}

?>