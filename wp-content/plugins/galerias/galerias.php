<?php

/*
Plugin Name: Galerias
Plugin URI: https://wordpress.com
Description: Um plugin criado para gerenciar galerias da intranet INEMA
Version: 0.1
Author: Filipe Lopes
Author URI: https://filipelopes.me
License: CC BY-NC 3.0 BR
Text Domain: galerias
*/

// Functions
include_once( plugin_dir_path( __FILE__ ) . 'functions.php' );

// API WP
include_once( plugin_dir_path( __FILE__ ) . 'rest_api.php' );

// Roles and capabilities for plugin
include_once( plugin_dir_path( __FILE__ ) . 'roles_cap.php' );

// Ação de executar o admin_menu no core do WORDPRESS 
add_action('admin_menu', 'menu_galerias');

function menu_galerias(){
    global $wpdb;
    global $wp_roles;

    // Adiciona menu na barra lateral
    // add_menu_page( string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '', string $icon_url = '', int $position = null )
    add_menu_page( 'Gerenciador de Álbuns', 'Álbuns', 'ascom_albuns' , 'albuns' , 'mostrar_galerias' , 'dashicons-format-gallery' , 20 );
    // add_submenu_page( string $parent_slug, string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '' )
    add_submenu_page( 'albuns', 'Criar novo álbum', 'Adicionar novo', 'ascom_albuns', 'galeria-criar', 'criar_galeria' );
    add_submenu_page( 'albuns', 'Editar álbum existente', 'Editar Álbum', 'ascom_albuns', 'galeria-editar', 'editar_galeria' );
    
    // Cria tabela que será usada no plugin
    $sql = "CREATE TABLE IF NOT EXISTS `wp_galerias` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `titulo` varchar(100) DEFAULT NULL,
        `observacoes` text DEFAULT NULL,
        `imagens` text DEFAULT NULL,
        `capa` int(11) DEFAULT NULL,
        `usuario` int(11) unsigned NOT NULL,
        `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `b_del` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`),
        KEY `usuario` (`usuario`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8, AUTO_INCREMENT=1";
    
    $wpdb->query($sql);
}

function mostrar_galerias(){
    include('pagina_principal.php');
}

function criar_galeria(){
    include('criar.php');
}

function editar_galeria(){
    include('editar.php');
}
