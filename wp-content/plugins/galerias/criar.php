<?php
$plugin_data = get_plugin_data( dirname( __FILE__ ) . '/galerias.php' );
$plugin_name = $plugin_data['Name'];
global $wpdb;

//novo

include_once( plugin_dir_path( __FILE__ ) . 'criar-processar.php' );

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>
    <h2 class="title">Para adicionar nova galeria preencha o formulário a seguir</h2>
    <form method="post" id="form_add_galerias" enctype="multipart/form-data">
        <table class="form-table">
            <tr>
                <th>Título</th>
                <td><input type="text" maxlength="150" name="titulo" id="titulo" required/><p class="description">Deve ter, no mínimo, 6 letras</p></td>
            </tr>
            <tr>
                <th>Para adicionar as imagens selecione uma das existentes na tabela abaixo abaixo ou faça o upload das fotos necessárias no botão abaixo e depois retorne aqui para adicioná-las à galeria criada: </th>
                <td><?=media_upload_form()?></td>
            </tr>
        </table>
        <input type="hidden" id="capa" name="capa"/> 
        <textarea id="textarea_galeria_imagens" name="imagens" style="display:none" required>[]</textarea>
        <h2 class="title">Lista de imagens</h2>
<table id="table_imagens" class="display">
    <thead>
        <tr>
            <th>Imagens</th>
            <th>Selecionar capa</th>
            <th>Nome do arquivo</th>
        </tr>
    </thead>
    <tbody>
    <?php
            $images = $wpdb->get_results("SELECT ID, post_name, `guid`, post_type, post_mime_type FROM wp_posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'image%' ");
            foreach($images as $image){
                echo "<tr>";
                echo "<td><input type=\"checkbox\" class=\"imagem_galeria\" name=\"imagem_galeria\" value=\"{$image->ID}\" id=\"img_{$image->post_name}\"/><label for=\"img_{$image->post_name}\"><img src=\"{$image->guid}\"/></label></td>";
                echo "<td><input type=\"radio\" name=\"cover\" value=\"{$image->ID}\"/></td>";
                echo "<td>{$image->post_name}</td>";
                echo "</tr>";
            }
        ?>
    </tbody>
</table>
<script>
jQuery(document).ready( function($){
    $('#table_imagens').DataTable();
}); 
</script>
        
        <br><br><input type="submit" value="Adicionar" class="button button-primary"/>
    </form>
</div>