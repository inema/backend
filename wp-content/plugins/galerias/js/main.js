jQuery(document).ready(function ($) {
  $("#table_imagens").on("change", ".imagem_galeria", function () {
    id = parseInt($(this).val());
    textarea = $("#textarea_galeria_imagens");
    json = JSON.parse(textarea.text());
    console.log(json);

    // Adiciona no campo
    if (this.checked) {
      json.push(id);
    } else {
      json.forEach(function (v, i) {
        if (id == v) {
          json.splice(i, 1);
        }
      })
    }
    textarea.text(JSON.stringify(json));
  })

  $("#table_imagens").on("change", "input[name=cover]", function () {
    $("#capa").val($(this).val());
  })

  $("#table_imagens").on("click", ".del_album", function (e) {
    e.preventDefault();
    if(confirm("Tem certeza que deseja excluir esse álbum?")){
      window.location.href = this.href;
    }
  })

})