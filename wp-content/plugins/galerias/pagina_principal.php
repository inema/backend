<?php
$plugin_data = get_plugin_data( dirname( __FILE__ ) . '/galerias.php' );
$plugin_name = $plugin_data['Name'];
$current_url="//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$edit_url=get_admin_url( null, '', 'admin' )."admin.php?page=galeria-editar";
global $wpdb;

include_once( plugin_dir_path( __FILE__ ) . 'excluir-processar.php' );

$userID = get_current_user_id();

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo $plugin_name ?></h1><a href="<?php echo get_admin_url( null, '', 'admin' ) ?>admin.php?page=galeria-criar" class="page-title-action">
    Criar nova</a><br />
    <p>Essas são os álbuns existentes, para editar ou excluir favor acessar na tabela a coluna de opções:</p>

    <form method="post" id="form_add_galerias" enctype="multipart/form-data">

<table id="table_imagens" class="display">
    <thead>
        <tr>
            <th>Nome da galeria</th>
            <th>Imagem de capa</th>
            <th>Data de criação</th>
            <th>Opções</th>
        </tr>
    </thead>
    <tbody>
    <?php
            $albuns = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}galerias WHERE b_del = 0");

            foreach($albuns as $album){
                echo "<tr>";
                echo "<td><img src=\"".wp_get_attachment_image_src($album->capa, "full")[0]."\"/></td>";
                echo "<td>{$album->titulo}</td>";
                echo "<td>{$album->id}</td>";
                echo "<td><a class=\"del_album\" href=\"{$current_url}&id={$album->id}\">Excluir</a> <a class=\"edt_album\" href=\"{$edit_url}&id={$album->id}\" >Editar</a></td>";
                echo "</tr>";
            }
        ?>
    </tbody>
</table>
<script>
jQuery(document).ready( function($){
    $('#table_imagens').DataTable();
}); 
</script>
        
        <br><br><input type="submit" value="Adicionar" class="button button-primary"/>
    </form>
</div>