<?php

// API WP
add_action('rest_api_init', function () {
  register_rest_route('intranet_config/v1', '/menus', array(
    'methods' => 'GET',
    'callback' => 'get_menus',
  ));
});

add_action('rest_api_init', function () {
  register_rest_route('intranet_config/v1', '/menus/intranet', array(
    'methods' => 'GET',
    'callback' => 'get_menu_intranet',
  ));
});

add_action('rest_api_init', function () {
  register_rest_route('intranet_config/v1', '/menus/sistemas', array(
    'methods' => 'GET',
    'callback' => 'get_menu_sistemas',
  ));
});

function get_menus()
{
  return array( 
    "info" => "Esse endpoint é destinado à captura de configurações e disposição de links dos menus principais da INTRANET",
    "routes" => array(
      "menu_intranet" => "intranet_config/v1/menus/intranet",
      "menu_sistemas" => "intranet_config/v1/menus/sistemas"
    )
  );
}


function get_menu_intranet($data)
{
  global $wpdb;

  $array_retorno = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}menus WHERE menu = 1 AND b_del = 0");

  return $array_retorno;
}

function get_menu_sistemas($data)
{
  global $wpdb;

  $temp_retorno = array();
  $id_destaque = 0;

  $items = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}menus WHERE menu = 2 AND b_del = 0");
  foreach($items as $item){
    // Não adiciona o título destaques, só grava o id para ver os filhos
    if($item->nome == "Destaques"){
      $id_destaque = $item->id;
    }else{
      array_push($temp_retorno, $item);
    }
  }

  $temp_destaque = array();

  foreach($temp_retorno as $key => $item){
    if($item->child_of == $id_destaque){
      array_push($temp_destaque, $item);
      unset($temp_retorno[$key]);
    }
  }
  
  $temp_retorno["destaques"] = $temp_destaque;
  $array_retorno = $temp_retorno;
  
  return $array_retorno;
}
