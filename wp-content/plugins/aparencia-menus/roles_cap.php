<?php

// Roles AND capabilities
function wporg_menus_role()
{
    add_role(
        'editor_menus',
        'Editor de Menus',
        [
            'read'         => true,
            'edit_posts'   => true,
        ]
    );
}
add_action('init', 'wporg_menus_role', 10);

function wporg_menus_role_caps()
{
    // gets the simple_role role object
    $role = get_role('editor_menus');
 
    // add a new capability
    $role->add_cap('ascom_menus', true);
}
 
// add simple_role capabilities, priority must be after the initial role definition
add_action('init', 'wporg_menus_role_caps', 11);

?>