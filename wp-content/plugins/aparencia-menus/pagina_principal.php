<?php

global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'editar-processar.php');

$userID = get_current_user_id();
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$base_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[PHP_SELF]?page=menus";

?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<span class='alert'></span>

<?php
    $menu_titles = array(
        "1" => "Menu Intranet",
        "2" => "Menu de Sistemas"
    );

    if(isset($_GET["menu"])){
        $get_menu_id = $_GET["menu"];
        function activeMenuClass($menu_id){
            if($_GET["menu"] == $menu_id){
                return "hover";
            }
        }

        $menu_title = $menu_titles[$get_menu_id];
    }
?>

<?php if(isset($_GET["menu"])){ ?>

<script>
    global_menu = <?=$_GET["menu"]?>    
</script>

<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>
    <a href="<?=$base_link.'&menu=1'?>" class="wp-action-bt-like <?=activeMenuClass(1)?>" role="button">Menu intranet</a>
    <a href="<?=$base_link.'&menu=2'?>" class="wp-action-bt-like <?=activeMenuClass(2)?>" role="button">Menu de Sistemas</a>

    <h2><?=$menu_title?></h2>
    <form method="post" id="form_edit_menus" action="<?=$actual_link?>">
    </form>

        <?php
            $menus = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}menus WHERE menu = $get_menu_id AND b_del = 0");
        ?>

        <textarea id="json_textarea_config_menus" name="json" cols="100" style="display:none">
            <?=json_encode($menus)?>
        </textarea>

        <?php
        $auto_increment_query = $wpdb->get_results("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{$wpdb->dbname}' AND TABLE_NAME = '{$wpdb->prefix}menus'");
        $auto_increment = $auto_increment_query[0]->AUTO_INCREMENT;
        ?>
        <input type="text" value="<?=$auto_increment?>" style="display:none" id="auto_increment"/> 
</div>

<?php }else{ ?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1>
    <a href="<?=$base_link.'&menu=1'?>" class="wp-action-bt-like" role="button">Menu intranet</a>
    <a href="<?=$base_link.'&menu=2'?>" class="wp-action-bt-like" role="button">Menu de Sistemas</a>
</div>

<?php } ?>