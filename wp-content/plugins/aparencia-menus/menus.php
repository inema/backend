<?php

/*
Plugin Name: Configuração Menus
Plugin URI: https://wordpress.com
Description: Um plugin criado para gerenciar os menus principais da INTRANET MEIOAMBIENTE INEMA/SEMA
Version: 0.1
Author: Filipe Lopes
Author URI: https://filipelopes.me
License: CC BY-NC 3.0 BR
Text Domain: menus
*/

// Functions
include_once( plugin_dir_path( __FILE__ ) . 'functions.php' );

// API WP
include_once( plugin_dir_path( __FILE__ ) . 'rest_api.php' );

// Roles and capabilities for plugin
include_once( plugin_dir_path( __FILE__ ) . 'roles_cap.php' );

// Ação de executar o admin_menu no core do WORDPRESS 
add_action('admin_menu', 'menu_menus');

function menu_menus(){
    global $wpdb;
    
    $menu_slug = "menus";
    $capability = "ascom_menus";

    // Adiciona menu no menu de configurações de aparência
    
    /* add_theme_page( 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '' ) */
    add_theme_page( 
        'Configuração de Menus', 
        'Menus', 
        $capability , 
        $menu_slug , 
        'mostrar_config_menus' );
    
    // Cria tabela que será usada no plugin
    $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}menus` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `menu` int(11) NOT NULL,
        `level` int(11) NOT NULL,
        `child_of` int(11) NOT NULL,
        `order` int(11) NOT NULL,
        `nome` varchar(100) NOT NULL,
        `src` varchar(300) NOT NULL,
        `b_del` tinyint(1) DEFAULT '0',
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    
    $wpdb->query($sql);
}

function mostrar_config_menus(){
    include('pagina_principal.php');
}