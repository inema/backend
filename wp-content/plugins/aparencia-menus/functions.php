<?php

// Adicionar scripts para rodar no dashboard

function plugin_menus_add_scripts_0125($hook) {
    // print_r($hook) appearance_page_menus
    if( $hook != "appearance_page_menus" ) return;
    wp_enqueue_script('plugin_menus_main_script', plugins_url('/js/main.js', __FILE__), array('jquery', 'jquery-ui-sortable'), '1.0', true);
}

add_action('admin_enqueue_scripts', 'plugin_menus_add_scripts_0125');

function plugin_menus_add_style_2354($hook) {
    if( $hook != "appearance_page_menus" ) return;
    wp_enqueue_style('plugin_menus_main_style', plugins_url('/css/main.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'plugin_menus_add_style_2354' );

?>