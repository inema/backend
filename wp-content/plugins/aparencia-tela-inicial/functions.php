<?php

// Adicionar scripts para rodar no dashboard

function plugin_tela_inicial_add_scripts_0125($hook) {
    // print_r($hook) appearance_page_tela_inicial
    if( $hook != "appearance_page_tela_inicial" ) return;
    wp_enqueue_script('plugin_tela_inicial_main_script', plugins_url('/js/main.js', __FILE__), array('jquery', 'jquery-ui-sortable'), '1.0', true);
}

add_action('admin_enqueue_scripts', 'plugin_tela_inicial_add_scripts_0125');

function plugin_tela_inicial_add_style_2354($hook) {
    if( $hook != "appearance_page_tela_inicial" ) return;
    wp_enqueue_style('plugin_tela_inicial_main_style', plugins_url('/css/main.css', __FILE__));
}
add_action( 'admin_enqueue_scripts', 'plugin_tela_inicial_add_style_2354' );

?>