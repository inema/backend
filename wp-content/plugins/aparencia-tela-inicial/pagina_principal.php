<?php

global $wpdb;

include_once(plugin_dir_path(__FILE__) . 'editar-processar.php');

$userID = get_current_user_id();

?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?=get_admin_page_title()?></h1><br />
    <p>Deslize os itens na ordem desejada e clique em atualizar</p>

    <form method="post" id="form_edit_tela_inicial">

        <ul class="sortable_jquery_ui">
            
            <?php
            $widgets = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}tela_inicial ORDER BY `order` ASC");
            foreach( $widgets as $widget ){
                $sortable = $widget->b_move ? "move" : "static" ; 
            ?>
            
            <li id="li_<?=$widget->id?>" class="<?=$sortable?>" data-order="<?=$widget->order?>" data-move="<?=$widget->b_move?>" data-newOrder="<?=$widget->order?>"><span><?=$widget->order?>. </span><?=$widget->titulo?></li>

            <?php
            }
            ?>

        </ul>
        
        <textarea id="json_textarea_config_tela_inicial" name="json" cols="100" style="display:none">
            <?=json_encode($widgets)?>
        </textarea>

        <br><br><input type="submit" value="Atualizar" class="button button-primary" />
    </form>
</div>