<?php

// API WP
add_action('rest_api_init', function () {
  register_rest_route('intranet_config/v1', '/tela_inicial', array(
    'methods' => 'GET',
    'callback' => 'get_tela_inicial',
  ));
});

function get_tela_inicial($data)
{
  global $wpdb;

  $array_retorno = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}tela_inicial WHERE b_show = 1 ORDER BY `order` ASC");

  return $array_retorno;
}
