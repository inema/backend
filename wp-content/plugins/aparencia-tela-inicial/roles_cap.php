<?php

// Roles AND capabilities
function wporg_tela_inicial_role()
{
    add_role(
        'editor_tela_inicial',
        'Editor de Tela Inicial',
        [
            'read'         => true,
            'edit_posts'   => true,
            'upload_files' => true,
        ]
    );
}
add_action('init', 'wporg_tela_inicial_role', 10);

function wporg_tela_inicial_role_caps()
{
    // gets the simple_role role object
    $role = get_role('editor_tela_inicial');
 
    // add a new capability
    $role->add_cap('ascom_tela_inicial', true);
}
 
// add simple_role capabilities, priority must be after the initial role definition
add_action('init', 'wporg_tela_inicial_role_caps', 11);

?>