<?php

/*
Plugin Name: Configuração de tela Inicial
Plugin URI: https://wordpress.com
Description: Um plugin criado para gerenciar a tela inicial da INTRANET MEIOAMBIENTE INEMA/SEMA
Version: 0.1
Author: Filipe Lopes
Author URI: https://filipelopes.me
License: CC BY-NC 3.0 BR
Text Domain: tela_inicial
*/

// Functions
include_once( plugin_dir_path( __FILE__ ) . 'functions.php' );

// API WP
include_once( plugin_dir_path( __FILE__ ) . 'rest_api.php' );

// Roles and capabilities for plugin
include_once( plugin_dir_path( __FILE__ ) . 'roles_cap.php' );

// Ação de executar o admin_menu no core do WORDPRESS 
add_action('admin_menu', 'menu_tela_inicial');

function menu_tela_inicial(){
    global $wpdb;
    
    $menu_slug = "tela_inicial";
    $capability = "ascom_tela_inicial";

    // Adiciona menu no menu de configurações de aparência
    
    /* add_theme_page( 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '' ) */
    add_theme_page( 
        'Configuração de disposição de itens na Tela Inicial', 
        'Tela inicial', 
        $capability , 
        $menu_slug , 
        'mostrar_config_tela_inicial' );
    
    // Cria tabela que será usada no plugin
    $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}tela_inicial` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `component` varchar(200) DEFAULT NULL,
        `slug` varchar(200) DEFAULT NULL,
        `titulo` varchar(200) DEFAULT NULL,
        `order` int(11) NOT NULL DEFAULT 99,
        `b_move` tinyint(1) NOT NULL DEFAULT '1',
        `b_show` tinyint(1) NOT NULL DEFAULT '1',
        PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8, AUTO_INCREMENT=1";
    
    $wpdb->query($sql);
}

function mostrar_config_tela_inicial(){
    include('pagina_principal.php');
}