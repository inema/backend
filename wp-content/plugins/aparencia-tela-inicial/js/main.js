jQuery(document).ready(function ($) {

  textarea = $("#json_textarea_config_tela_inicial");
  json_textarea = JSON.parse(textarea.text()) || null;
  temp = [];

  const writeItem = e => {
    sortable = parseInt(e.bMove) ? "move" : "static" ;
    $( ".sortable_jquery_ui" ).append(`
    <li id="li_${e.id}" class="${sortable}" data-order="${e.order}" data-move="${e.bMove}" data-newOrder="${e.newOrder}"
      <span>${e.newOrder}. </span>
    ${e.titulo} </li> `);
  }

  const reWriteList = () => {
    $( ".sortable_jquery_ui" ).html("");
    
    json_textarea.forEach( e => {
      writeItem(e)
    })

  }

  const checkOrder = (prev, next) => {
    /*
    Função que verifica se por acidente ou consequencia algum item que não devia se mover saiu do lugar
    prev = index prévio do item que acabou de ser mexido
    next = próximo index do item que acabou de ser mexido 
    */

    console.log("prev, next", prev, next);

    //  $( ".sortable_jquery_ui" ).html("");

    temp = json_textarea;

    temp.forEach( (e, i, a) => {
      // Verifica se existia, nessa posição algum que não devia se mover
      /* Primeiro verifico se naquela ordem existem alguem com a ordem original sem move, ou seja, que não devia se mover. Caso haja, ele é prioridade */
      if(e.order != e.newOrder && !parseInt(e.bMove) ){
        console.log("DEVE REVER. devia estar em", e.order, "mas está em", e.newOrder)
        
        // verifica quem estava no lugar dele
        wrongElem = 99,
        temp.forEach( (el, idx, arr) => {
          if(el.newOrder == e.order){
            wrongElem = el.id

            // se o movimento foi para cima do drag, o wrongElem tem que ir para baixo
            if(prev > next){
              // movimento para cima
              arr[idx].newOrder = e.order+1
            }else{
              // movimento para baixo
              arr[idx].newOrder = e.order-1
            }
          }
        })

        console.log($("#li_"+wrongElem))

        // Atribui a ordem correta novamente a ele
        a[i].newOrder = e.order;

      }else if(e.id == parseInt(e.id)){
        // Caso seja um estático forade posição não deve mexer
        console.log("PODE MUDAR TUDO")
        // writeItem(e);
      }
    })

    // Reorganizar array com base na ordem
    temp.sort( function(a, b){
      if( a.newOrder > b.newOrder ) return 1
      if( a.newOrder < b.newOrder ) return -1
      return 0
    })
    textarea.text(JSON.stringify(temp));
    reWriteList();

  }

  const updateOrder = ( e, ui ) => {
    // Captura variável de item estático ou não para o item que acabou de ser movido
    shouldMove = parseInt(ui.item.attr("data-move"));

    // capturar valors de campo para variavel teporaria
    temp = json_textarea;

    //  Verifica se está tentando movimentar um item estático
    if(!shouldMove){
      $( ".sortable_jquery_ui" ).sortable( "cancel" )
      return true
    }

    // Passeia em todos os itens da nova listagem
    orderedIds = $( ".sortable_jquery_ui" ).sortable( "toArray" )
    orderedIds.forEach( (e, i) => {
      jsonId = parseInt($("#"+e).attr("id").replace(/li_/g,''))-1;
      temp[jsonId].newOrder = i+1;
    });

    // Reorganizar array com base na ordem
    temp.sort( function(a, b){
      if( a.newOrder > b.newOrder ) return 1
      if( a.newOrder < b.newOrder ) return -1
      return 0
    })
    
    textarea.text(JSON.stringify(temp));

    checkOrder(ui.item[0].dataset.order, ui.item.index());
    
    console.log(temp)
  }

  // Reescrevo valores do BD para que o json fique mais limpo
  reWriteJson = () => {
    json_textarea.forEach( (e, i, a) => {
      a[i] = {
        id: parseInt(e.id),
        order: parseInt(e.order),
        newOrder: parseInt(e.order),
        titulo: e.titulo,
        bMove: parseInt(e.b_move),
        bShow: parseInt(e.b_show)
      }
    })
    textarea.text(JSON.stringify(json_textarea));
  }

  // Inicialização do gerenciador
  initItens = () => {
    $( ".sortable_jquery_ui" ).sortable({
      update: updateOrder
    });

    reWriteJson();
  }

  initItens();

})