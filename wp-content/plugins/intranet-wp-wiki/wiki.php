<?php

/*
Plugin Name: Wiki
Plugin URI: https://wordpress.com
Description: Um plugin criado escrever a forma de utilizar as funções do painel WORDPRESS para gerenciamento da intranet
Version: 0.1
Author: Filipe Lopes
Author URI: https://filipelopes.me
License: CC BY-NC 3.0 BR
Text Domain: wiki
*/

// Ação de executar o admin_menu no core do WORDPRESS 
add_action('admin_menu', 'menu_wiki');

function menu_wiki(){
    global $wpdb;
    global $wp_roles;

    $menu_slug = "wiki";
    $capability = "read";

    // Adiciona menu na barra lateral
    
    /* add_menu_page( 
        string $page_title, 
        string $menu_title, 
        string $capability, 
        string $menu_slug, 
        callable $function = '', 
        string $icon_url = '', 
        int $position = null ) */
    add_menu_page( 
        'Wiki', 
        'Wiki', 
        $capability , 
        $menu_slug , 
        'mostrar_wiki' , 
        'dashicons-book-alt' , 
        90 );
}

function mostrar_wiki(){
    include('pagina_principal.php');
}