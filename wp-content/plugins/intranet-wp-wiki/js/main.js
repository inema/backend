jQuery(document).ready(function ($) {

  $("#table_imagens").on("change", "input[name=imagem_banner]", function () {
    $("#input_imagem_banner").val($(this).val());
  })

  $("#table_imagens").on("click", ".del_album", function (e) {
    e.preventDefault();
    if(confirm("Tem certeza que deseja excluir esse álbum?")){
      window.location.href = this.href;
    }
  })

})