<?php

$img_path = plugin_dir_url( __FILE__ ) . "imagens"

?>

<h2> Como atualizar os menus da Intranet? </h2>
<p> Os menus são os facilitadores de acesso aos conteúdos, na Intranet há dois distintos menus: 
<ul style="list-style-type:disc;">
  <li> Menu da intranet - responsável pelo acesso aos conteúdos postados;  </li> </br>
<!--   <img src="<?=$img_path?>/menu_intranet.png" align="middle"> </br> -->
  <li> Menu de Sistemas - responsável pelo acesso aos sistemas da SEMA/INEMA.</li> </br>
<!--   <img src="<?=$img_path?>/menu_sistemas.png" align="middle"> </br> -->
</ul> 
</p>

<p>	Para realizar a atualização dos Menus, o usuário deve selecionar a opção "Aparência", "Menus" localizado no Menu principal ( imagem abaixo).</p>
<p>
<img src="<?=$img_path?>/gerenciador_menus.png" align="middle">
</p>
</br>
<p>Ao acessar as configurações de Menus o usuário pode editar ou adicionar novos itens e/ou categorias. </p>
</br>
<h4>Categoria</h4>
<p>Para adicionar um nova categoria o usuário deve clicar no botão, <img src="<?=$img_path?>/novas_categorias.png" align="middle">, localizado à esquerda na parte inferior da página de Configuração de Menus. </p>
<p>O usuário deve preencher o nome da Categoria no campo, <img src="<?=$img_path?>/nome_categoria.png" align="middle">, um link em <img src="<?=$img_path?>/nome_link.png" align="middle">, esse campo não é obrigatório e por fim a permissão de visualização desta categoria. O padrão de visualização da categoria é visível pelo Inema e pela Sema <img src="<?=$img_path?>/permissao_de_visualizacao.png" align="middle"> para alterar essa visualização o usuário deve alterar na lista qual o órgão pode visualizar a categoria. </p>
<p>Para ordenar as Categorias o usuário deve alterar o posicionamento delas através das setas que estão localizadas ao lado da categoria. Somente é permitido a alteração da posição no sentido vertical. <img src="<?=$img_path?>/setas_vertical.png" align="middle"> </p>
<p> Para finalizar a inserção da Categoria o usuário deve confirmar a ação clicando no botão <img src="<?=$img_path?>/atualizar.png" align="middle"> localizado ao final da página. </p>

<h4>Item</h4>
<p> Para adicionar uma novo item a uma categoria o usuário deve clicar no botão, <img src="<?=$img_path?>/novos_itens.png" align="middle"> que fica localizado dentro da seção de cada categoria. </p>
<p>O usuário deve preencher o nome do Item no campo, <img src="<?=$img_path?>/nome_novo_item.png" align="middle">, um link em <img src="<?=$img_path?>/link_do_item.png" align="middle">, esse campo não é obrigatório e por fim a permissão de visualização deste item. O padrão de visualização da item é visível pelo Inema e pela Sema <img src="<?=$img_path?>/permissao_de_visualizacao.png" align="middle"> para alterar essa visualização o usuário deve alterar na lista qual o órgão pode visualizar o item. </p>
<p>Para ordenar os itens o usuário deve alterar o posicionamento deles através das setas que estão localizadas ao lado da item.</p>
<p>A movimentação das setas no sentido horizontal determina se é um item ou subitem. A profundidade máxima de um sub-item é de até três níveis (imagem abaixo):</p>
<p><img src="<?=$img_path?>/subsub-item.png" align="middle"> </p>

<p>Para alterar a ordem de um item ou subitem o usuário deve alterar o posicionamento deles através das setas verticais que estão localizadas ao lado da categoria. <img src="<?=$img_path?>/setas_vertical.png" align="middle"> </p>

<h2> Como criar um link para páginas internas nos menus? </h2>
<p> Ao criar uma página interna na Intranet um link é gerado para cada página, como podemos observar na área destacada na imagem abaixo.</p>
<img src="<?=$img_path?>/criar_pagina.png" align="middle"> 
<p> O usuário deve clicar no botão "Editar", que fica localizado ao lado do link gerado, e copiar o texto. A parte final do link é usado como atalho para as páginas internas do sistema.</p>
<p> Para adicionar ao menu um link para a página interna o usuário deve copiar o final do link como foi explicado acima e colar o texto no espaço destinado a link de Categoria ou Item, exemplificado na imagem abaixo:</p>
<img src="<?=$img_path?>/link_categoria_item.png" align="middle"> 

<h2> Como não exibir a data de aniversário ou foto de um colaborador? </h2>

<p>Para que não seja exibida a foto e/ou a data de aniversário de um colaborador, o usuário deve no cadastramento do colaborador ou na edição desmarcar os checkbox "Mostrar foto" e "Mostrar aniversário", destacados na imagem abaixo:</p>
<img src="<?=$img_path?>/colaborador.png" align="middle">
<p> Para confirmar que as informações não estão sendo exibidas, no Gerenciador de Colaborador, ao listar as informações dos colaboradores a foto do colaborador e data de nascimento deve ser ter as bordas na coloração vermelha(imagem abaixo).</p>
<img src="<?=$img_path?>/bordas_colaborador.png" align="middle">


