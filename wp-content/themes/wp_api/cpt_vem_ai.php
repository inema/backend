<?php

/* ----------
	Adicionando custom_post_type VEM AÍ
	register_post_type( string $post_type, array|string $args = array() )
---------- */

// Faltando implementar galeria

add_action( 'init', 'register_cpt_vem_ai' );

function register_cpt_vem_ai(){
    register_post_type('vem-ai',
        array(
            'labels' => array(
                'name'          =>  'Vem aí',
                'singular_name' =>  'Evento',
                'menu_name'     =>  'Vem aí',
                'all_items'     =>  'Todos',
                'add_new'       =>  'Adicionar novo',
                'add_new_item'  =>  'Adicionar novo evento'
                ),
            'menu_position' => 4,
            'public'    => true,
            'supports'  => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'post-formats',
                            'excerpt',
                            'custom-fields'
                            ),
            'show_in_admin_bar' =>  true,
            'taxonomies' => array('post_tag'),
            'menu_icon' => 'dashicons-calendar',
            'show_in_rest' => true,
            // 'capabilities' => array()
            'register_meta_box_cb' => 'vem_ai_meta_box'
        )
    );
}

/* ---------
    Opções:
    add_meta_box( 
        string $id, 
        string $title, 
        callable $callback, 
        string|array|WP_Screen $screen = null, 
        string $context = 'advanced', 
        string $priority = 'default', 
        array $callback_args = null 
    )
---------- */

function vem_ai_meta_box() {
    add_meta_box(
        '_data_hora',
        __( 'Data e hora', 'data_hora' ),
        'data_hora_cb',
        'vem-ai',
        'side',
        'high'
    );

    add_meta_box(
        '_local',
        __( 'Local', 'local' ),
        'local_cb',
        'vem-ai',
        'side',
        'high'
    );
}

function data_hora_cb( $post ) {
    $value = get_post_meta( $post->ID, '_data_hora', true );
    echo '
    <label for="data_hora">Selecione a data e hora do início do evento</label>
    <input type="datetime-local" style="width:100%" id="data_hora" name="data_hora" value="' . esc_attr( $value ) . '"/>';
}

/* ----------
    Scripts que serão utilizados no metabox LOCAL para busca
---------- */

$hook_list_mtbx_local = array(
    "post-new.php"
);

function mtbx_local_add_scripts_0125($hook) {
    // print_r($hook); // toplevel_page_banners
    global $hook_list_mtbx_local;
    if( !in_array($hook, $hook_list_mtbx_local) ) return;
    wp_enqueue_script('jquery-ui-autocomplete');
}

add_action('admin_enqueue_scripts', 'mtbx_local_add_scripts_0125');

function local_cb( $post ) {
    global $wpdb;
    
    $value = get_post_meta( $post->ID, '_local', true );
    echo '
    <label for="local">Selecione a data e hora do início do evento</label>
    <input type="text" style="width:100%" id="local" name="local" value="' . esc_attr( $value ) . '"/>';

    $locais = $wpdb->get_col("SELECT DISTINCT meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '_local' ORDER BY `meta_value` ASC");
    $locais = array_values(array_filter($locais));
    $source_autocomplete_locais = json_encode($locais);
    ?>

    <script>
        jQuery(document).ready(function ($) {
            $("#local").autocomplete({
                source: <?=$source_autocomplete_locais?>
            })

            console.log(<?=$source_autocomplete_locais?>)
        })
    </script>

    <?php
}

function save_data_hora( $post_id ) {

    /* ----------
    beautiful code from https://www.sitepoint.com/adding-meta-boxes-post-types-wordpress/
    ---------- */

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['data_hora'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_data_hora', $my_data );
}

add_action( 'save_post', 'save_data_hora' );

function save_local( $post_id ) {

    /* ----------
    beautiful code from https://www.sitepoint.com/adding-meta-boxes-post-types-wordpress/
    ---------- */

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['local'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_local', $my_data );
}

add_action( 'save_post', 'save_local' );

/* ----------
Registro para metavalues funcionarem com WP API
---------- */

add_action( 'init', 'register_posts_vem_ai_meta_field' );

function register_posts_vem_ai_meta_field() {

    register_meta( 'post', '_data_hora', array(
        'object_subtype' => 'vem-ai',
        'type'           => 'string',
        'description'    => 'A meta key associated with a string meta value.',
        'single'         => true,
        'show_in_rest'   => true,
    ));

    register_meta( 'post', '_local', array(
        'object_subtype' => 'vem-ai',
        'type'           => 'string',
        'description'    => 'A meta key associated with a string meta value.',
        'single'         => true,
        'show_in_rest'   => true,
    ));

}


?>