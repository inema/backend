<?php

/* ----------
	Adicionando custom_post_type ALTOS PAPOS
	register_post_type( string $post_type, array|string $args = array() )
---------- */

add_action( 'init', 'register_cpt_altos_papos' );

function register_cpt_altos_papos(){
    register_post_type('altos-papos',
        array(
            'labels' => array(
                'name'          =>  'Altos Papos',
                'singular_name' =>  'Artigo',
                'menu_name'     =>  'Altos Papos',
                'all_items'     =>  'Todos',
                'add_new'       =>  'Adicionar novo',
                'add_new_item'  =>  'Adicionar nova postagem \'Altos Papos\''
                ),
            'menu_position' => 4,
            'public'    => true,
            'supports'  => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'post-formats',
                            'excerpt',
                            'custom-fields'
                            ),
            'show_in_admin_bar' =>  true,
            'taxonomies' => array('post_tag'),
            'menu_icon' => 'dashicons-format-chat',
            'show_in_rest' => true,
            // 'capabilities' => array()
            'register_meta_box_cb' => 'altos_papos_meta_box'

        )
    );
}

function altos_papos_meta_box() {
    add_meta_box(
        '_nome_entrevistado',
        __( 'Nome do autor/entrevistado', 'nome' ),
        'nome_entrevistado_callback',
        'altos-papos',
        'side',
        'high'
    );
}

function nome_entrevistado_callback( $post ) {
    $value = get_post_meta( $post->ID, '_nome_entrevistado', true );
    echo '<input type="text" style="width:100%" id="nome_entrevistado" name="nome_entrevistado" value="' . esc_attr( $value ) . '"/>';
}

function save_nome_entrevistado( $post_id ) {

    /* ----------
    beautiful code from https://www.sitepoint.com/adding-meta-boxes-post-types-wordpress/
    ---------- */

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['nome_entrevistado'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_nome_entrevistado', $my_data );
}

add_action( 'save_post', 'save_nome_entrevistado' );

add_action( 'init', 'register_posts_altos_papos_meta_field' );

function register_posts_altos_papos_meta_field() {

    register_meta( 'post', '_nome_entrevistado', array(
        'object_subtype' => 'altos-papos',
        'type'           => 'string',
        'description'    => 'A meta key associated with a string meta value.',
        'single'         => true,
        'show_in_rest'   => true,
    ));

}

/* ----------
    Editando resposta padrão do REST API para altos-papos
---------- */

require_once get_template_directory().'/vendor/autoload.php';
use ColorThief\ColorThief;

function altos_papos_get_image_color( $post, $field_name, $request ) {
    if($post["featured_media"]){
        $palette = ColorThief::getPalette(wp_get_attachment_image_src($post["featured_media"], "full")[0], 8);

        return $palette;
    }else{
        return null;
    }
  }
   
  function altos_papos_add_image_color() {
    register_rest_field( 'altos-papos',
      'palette',
      array(
        'get_callback' => 'altos_papos_get_image_color'
      )
    );
  }
  add_action( 'rest_api_init', 'altos_papos_add_image_color' );


?>