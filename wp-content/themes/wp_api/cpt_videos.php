<?php

/* ----------
	Adicionando custom_post_type ALTOS PAPOS
	register_post_type( string $post_type, array|string $args = array() )
---------- */

add_action( 'init', 'register_cpt_videos' );

function register_cpt_videos(){
    register_post_type('videos',
        array(
            'labels' => array(
                'name'          =>  'Vídeos',
                'singular_name' =>  'Artig',
                'menu_name'     =>  'Vídeos',
                'all_items'     =>  'Todos',
                'add_new'       =>  'Adicionar novo',
                'add_new_item'  =>  'Adicionar nova postagem de Vídeo'
                ),
            'menu_position' => 4,
            'public'    => true,
            'supports'  => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'post-formats',
                            'excerpt',
                            'custom-fields'
                            ),
            'show_in_admin_bar' =>  true,
            'taxonomies' => array('post_tag'),
            'menu_icon' => 'dashicons-format-video',
            'show_in_rest' => true,
            // 'capabilities' => array()
            'register_meta_box_cb' => 'videos_meta_box',

        )
    );
}

function videos_meta_box() {
    add_meta_box(
        '_videos',
        __( 'Vídeos da Galeria', 'videos' ),
        'videos_callback',
        'videos',
        'advanced',
        'high'
    );
}

function videos_callback( $post ) {
    
    global $wpdb;
    
    // $value = get_post_meta( $post->ID, '_videos', true );
    // echo '<input type="text" style="width:100%" id="videos" name="videos" value="' . esc_attr( $value ) . '"/>';

    $value = get_post_meta( $post->ID, '_video', true );
    // echo '
    // <label for="videos">Selecione a data e hora do início do evento</label>
    // <input type="text" style="width:100%" id="videos" name="videos" value="' . esc_attr( $value ) . '" autocomplete="false"/>';

    ?> 

    <section id="videos_mtbx">

    <?php

    $all_videos = $wpdb->get_results("SELECT ID, post_name, `guid`, post_type, post_mime_type FROM wp_posts WHERE post_type = 'attachment' AND post_mime_type LIKE 'video%' ORDER BY ID DESC ");
    foreach($all_videos as $video){
        $checked = $video->ID === $value ? "checked" : "";
        echo "<input type=\"radio\" name=\"video\" id=\"ft_gall_{$video->ID}\" class=\"checkbox_ft_gall\" value=\"{$video->ID}\" {$checked} >";
        echo "<label class=\"mtbx_ft_gall_img\" for=\"ft_gall_{$video->ID}\">";
        echo "<video>
            <source type=\"{$video->post_mime_type}\" src=\"{$video->guid}\"/>
            </video>";
        echo "</label>";
        // print_r($video);
    }
    ?>

    </section>

    <?php
}

function save_videos( $post_id ) {

    /* ----------
    beautiful code from https://www.sitepoint.com/adding-meta-boxes-post-types-wordpress/
    ---------- */

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['video'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_video', $my_data );
    update_post_meta( $post_id, '_video_url', wp_get_attachment_url($my_data) );
}

add_action( 'save_post', 'save_videos' );

add_action( 'init', 'register_posts_videos_meta_field' );

function register_posts_videos_meta_field() {

    register_meta( 'post', '_video', array(
        'object_subtype' => 'videos',
        'type'           => 'string',
        'description'    => 'A meta key associated with a string meta value.',
        'single'         => true,
        'show_in_rest'   => true,
    ));

    register_meta( 'post', '_video_url', array(
        'object_subtype' => 'videos',
        'type'           => 'string',
        'description'    => 'A meta key associated with a string meta value.',
        'single'         => true,
        'show_in_rest'   => true,
    ));

}


?>