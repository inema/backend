<?php

/* ----------
	Adicionando custom_post_type ALTOS PAPOS
	register_post_type( string $post_type, array|string $args = array() )
---------- */

add_action( 'init', 'register_cpt_fica_a_dica' );

function register_cpt_fica_a_dica(){
    register_post_type('fica-a-dica',
        array(
            'labels' => array(
                'name'          =>  'Fica a Dica',
                'singular_name' =>  'Artigo',
                'menu_name'     =>  'Fica a Dica',
                'all_items'     =>  'Todos',
                'add_new'       =>  'Adicionar novo',
                'add_new_item'  =>  'Adicionar nova postagem \'Fica a Dica\''
                ),
            'menu_position' => 4,
            'public'    => true,
            'supports'  => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'post-formats',
                            'excerpt',
                            'custom-fields'
                            ),
            'show_in_admin_bar' =>  true,
            'taxonomies' => array('post_tag'),
            'menu_icon' => 'dashicons-lightbulb',
            'show_in_rest' => true,
            // 'capabilities' => array()
            'register_meta_box_cb' => 'fica_a_dica_meta_box',

        )
    );
}

function fica_a_dica_meta_box() {
    add_meta_box(
        '_categoria_fad',
        __( 'Categoria da dica', 'categoria_fad' ),
        'categoria_fad_callback',
        'fica-a-dica',
        'side',
        'high'
    );
}

function categoria_fad_callback( $post ) {
    
    global $wpdb;
    
    // $value = get_post_meta( $post->ID, '_categoria_fad', true );
    // echo '<input type="text" style="width:100%" id="categoria_fad" name="categoria_fad" value="' . esc_attr( $value ) . '"/>';

    $value = get_post_meta( $post->ID, '_categoria_fad', true );
    echo '
    <label for="categoria_fad">Selecione a data e hora do início do evento</label>
    <input type="text" style="width:100%" id="categoria_fad" name="categoria_fad" value="' . esc_attr( $value ) . '" autocomplete="false"/>
    <p>Sugestões: Filmes, ...</p>';

    $categorias = $wpdb->get_col("SELECT DISTINCT meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '_categoria_fad' ORDER BY `meta_value` ASC");
    $categorias = array_values(array_filter($categorias));
    $source_autocomplete_categorias = json_encode($categorias);
    ?>

    <script>
        jQuery(document).ready(function ($) {
            $("#categoria_fad").autocomplete({
                source: <?=$source_autocomplete_categorias?>
            })

            console.log(<?=$source_autocomplete_categorias?>)
        })
    </script>

    <?php
}

function save_categoria_fad( $post_id ) {

    /* ----------
    beautiful code from https://www.sitepoint.com/adding-meta-boxes-post-types-wordpress/
    ---------- */

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['categoria_fad'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_categoria_fad', $my_data );
}

add_action( 'save_post', 'save_categoria_fad' );

add_action( 'init', 'register_posts_fica_a_dica_meta_field' );

function register_posts_fica_a_dica_meta_field() {

    register_meta( 'post', '_categoria_fad', array(
        'object_subtype' => 'fica-a-dica',
        'type'           => 'string',
        'description'    => 'A meta key associated with a string meta value.',
        'single'         => true,
        'show_in_rest'   => true,
    ));

}


?>