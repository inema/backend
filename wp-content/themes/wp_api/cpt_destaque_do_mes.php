<?php

/* ----------
	Adicionando custom_post_type DESTQUE DO MÊS
	register_post_type( string $post_type, array|string $args = array() )
---------- */

add_action( 'init', 'register_cpt_destaque_do_mes' );

function register_cpt_destaque_do_mes(){
    register_post_type('destaque-do-mes',
        array(
            'labels' => array(
                'name'          =>  'Destaque do Mês',
                'singular_name' =>  'Artigo',
                'menu_name'     =>  'Destaque do Mês',
                'all_items'     =>  'Todos',
                'add_new'       =>  'Adicionar novo',
                'add_new_item'  =>  'Adicionar novo artigo'
                ),
            'menu_position' => 4,
            'public'    => true,
            'show_in_admin_bar' =>  true,
            'supports'  => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'post-formats',
                            'excerpt'
                            ),
            'taxonomies' => array('post_tag'),
            'menu_icon' => 'dashicons-awards',
            'show_in_rest' => true,
            'capability_type' => 'ascom_destaque_do_mes',
            'capabilities' => array( 
                'publish_posts' => 'ascom_destaque_do_mes',
                'edit_posts' => 'ascom_destaque_do_mes',
                'edit_others_posts' => 'ascom_destaque_do_mes',
                'read_private_posts' => 'ascom_destaque_do_mes',
                'edit_post' => 'ascom_destaque_do_mes',
                'delete_post' => 'ascom_destaque_do_mes',
                'read_post' => 'ascom_destaque_do_mes'
            )

        )
    );
}

// Roles AND capabilities
function wporg_cpt_destaque_do_mes_role()
{
    add_role(
        'editor_destaque_dos_mes',
        'Editor Destaque do Mês',
        [
            'read'         => true,
            'edit_posts'   => true,
            'upload_files' => true,
        ]
    );
}

add_action('init', 'wporg_cpt_destaque_do_mes_role', 10);

function wporg_cpt_destaque_do_mes_role_caps()
{
    $role = get_role('editor_destaque_dos_mes');
    $role->add_cap('ascom_destaque_do_mes', true);
}
 
add_action('init', 'wporg_cpt_destaque_do_mes_role_caps', 11);


?>