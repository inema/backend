<?php

/* ----------
	Adicionando custom_post_type VER DE DENTRO
	register_post_type( string $post_type, array|string $args = array() )
---------- */

add_action( 'init', 'register_cpt_ver_de_dentro' );

function register_cpt_ver_de_dentro(){
    register_post_type('ver-de-dentro',
        array(
            'labels' => array(
                'name'          =>  'Ver de Dentro',
                'singular_name' =>  'Artigo',
                'menu_name'     =>  'Ver de Dentro',
                'all_items'     =>  'Todos',
                'add_new'       =>  'Adicionar novo',
                'add_new_item'  =>  'Adicionar novo artigo'
                ),
            'menu_position' => 4,
            'public'    => true,
            'supports'  => array(
                            'title',
                            'editor',
                            'thumbnail',
                            'post-formats',
                            'excerpt',
                            'custom-fields'
                            ),
            'show_in_admin_bar' =>  true,
            'taxonomies' => array('post_tag'),
            'menu_icon' => 'dashicons-grid-view',
            'show_in_rest' => true,
            // 'capabilities' => array()

        )
    );
}


?>